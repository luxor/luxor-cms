#luxor-cms 信息/权限管理系统。
> 这是一个基于Java语言编写,集成SpringMVC4+JPA2.0+Hibernate+Shiro+Bootstrap+Require.js等框架搭建而成的企业快速开发框架。
> 
> 使用过程有任何问题，请联系：705673261@qq.com 
>
> 后台效果图：
![后台首页真实效果图](https://gitee.com/uploads/images/2017/1018/223151_5914e1ff_121257.png "index.png")