FROM tomcat:8.5.34-jre8-slim

#定义配置文件目录
ENV WORK_PATH /usr/local/tomcat
#定义war运行目录
ENV WAR_PATH /usr/local/tomcat/webapps
#war文件
ENV APP_NAME target/*.war
#Jvm优化参数
ENV JAVA_OPTS -Xmx128m -Xss256k

EXPOSE 80

#VOLUME
VOLUME ["/usr/local/tomcat/logs", "/tmp/tomcat/logs"]

# WORKDIR
WORKDIR $WORK_PATH

#JVM SETTING
RUN export CATALINA_OPTS="$JAVA_OPTS"

#GMT+8
RUN echo "Asia/Shanghai" > /etc/timezone

#删除多余项目
RUN rm -rf $WAR_PATH/*

#复制WAR包到容器
COPY $APP_NAME $WAR_PATH/ROOT.war
#复制server.xml到容器
COPY server.xml $WORK_PATH/conf/

# ENTRYPOINT


# Dockerfile使用说明
#
# 1.制作镜像
# docker build -t ccr.ccs.tencentyun.com/dqsf/dqsf-share-auth .
#
# 2.推送到仓库(密码：do1admin)
# docker login --username=100003422040 ccr.ccs.tencentyun.com
# docker push ccr.ccs.tencentyun.com/dqsf/dqsf-share-auth:latest
#
# 3.启动容器
# docker run --restart always -d -p 80:8080 --hostname dqsf-share-auth-1 -e "SPRING_PROFILES_ACTIVE=local" --name dqsf-share-auth-1 ccr.ccs.tencentyun.com/dqsf/dqsf-share-auth:latest
# docker run --restart always -d -p 81:8080 --hostname dqsf-share-auth-2 -e "SPRING_PROFILES_ACTIVE=local" --name dqsf-share-auth-2 ccr.ccs.tencentyun.com/dqsf/dqsf-share-auth:latest

