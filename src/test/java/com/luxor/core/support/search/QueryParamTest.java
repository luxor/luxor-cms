package com.luxor.core.support.search;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.luxor.core.plugin.search.QueryParam;
import com.luxor.core.plugin.search.SearchHolder;

public class QueryParamTest
{
	@Test
	public void test()
	{
		List<QueryParam> filters = new ArrayList<QueryParam>();
		// 过滤集合
		String[] groups = new String[] { "AND","GE", "age","AND", "LE", "age" };
		QueryParam qp = new QueryParam("AND", groups, "18,100");

		filters.add(qp);
		String sql = SearchHolder.getFilterSql("sysUser", filters);
		System.out.println(sql);
		for (Object val :SearchHolder.getQueryValues(filters)) {
			System.out.println(val);
		}
	}

	@Test
	public void single()
	{
		List<QueryParam> filters = new ArrayList<QueryParam>();
		//
		QueryParam param1 = new QueryParam("name", "EQ", "xinmingyan");
		QueryParam param2 = new QueryParam("age", "GE", "18");
		filters.add(param1);
		filters.add(param2);
		String sql = SearchHolder.getFilterSql("sysUser", filters);
		System.out.println(sql);
	}
}
