package com.luxor.cms.util;

import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.luxor.core.entity.sys.SysUser;
import com.luxor.core.repository.sys.SysUserDao;

@ComponentScan
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class SysConfigServiceTest
{
	@Resource
	private SysUserDao sysUserDao;

	@Test
	public void findCustomer()
	{
		EntityManager em = sysUserDao.getEntityManager();
		String jpql = "FROM SysUser u";
		@SuppressWarnings("unchecked")
		List<SysUser> result = em.createQuery(jpql).getResultList();
		for (SysUser sysUser : result)
		{
			System.out.println(sysUser.toString());
		}
	}
}
