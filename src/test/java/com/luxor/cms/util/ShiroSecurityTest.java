package com.luxor.cms.util;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.luxor.core.plugin.shiro.realm.MyJdbcRealm;

@ComponentScan
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class ShiroSecurityTest
{
	@Resource(name = "userRealm")
	MyJdbcRealm userRealm;

	@Test
	public void testLogin() throws Exception
	{
		boolean testResult = true;
		// 1、准备securityManager安全管理器,并绑定到Shiro的全局工具类
		DefaultSecurityManager securityManager = new DefaultSecurityManager();
		securityManager.setRealm(userRealm);

		SecurityUtils.setSecurityManager(securityManager);
		// 2、准备token身份凭证
		UsernamePasswordToken token = new UsernamePasswordToken("root", "root");
		// 3、获取当前要登陆的用户
		Subject subject = SecurityUtils.getSubject();
		try
		{
			// 4、登陆
			subject.login(token);
		} catch (Exception ex)
		{
			testResult = false;
			throw ex;
		}
		// 5、退出
		subject.logout();
		Assert.assertTrue("Error,testResult= false", testResult);
	}
}
