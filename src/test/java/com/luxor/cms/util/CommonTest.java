package com.luxor.cms.util;

import java.lang.reflect.Method;
import java.lang.reflect.Type;

import org.junit.Test;
import org.springframework.util.SocketUtils;

public class CommonTest
{

	class Re
	{
		public void test()
		{
			System.out.println("....");
		}
	}

	@Test
	public void returnType()
	{
		CommonTest.Re re = new CommonTest.Re();
		Method[] methods = re.getClass().getMethods();
		for (Method m : methods)
		{
			Type type = m.getGenericReturnType();
			System.out.println(type.toString());
		}
	}

	@Test
	public void socket()
	{
		SocketUtils.findAvailableTcpPort(3306);
		SocketUtils.findAvailableTcpPort(3000, 3306);
		SocketUtils.findAvailableUdpPort(3306);
	}

}
