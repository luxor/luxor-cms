#!/bin/bash
############################################################
# 自动监控tomcat脚本并且执行重启操作。
# param1:tomcat安装目录(如：/home/guest/tomcat5.5)。
# param2:应用的唯一标识名称(如:luxor.cms.node01)。
# execut /home/guest/luxor/bin/immortal.sh /home/guest/tomcat5.5 luxor.cms.node01
#
# @auditor XinmingYan 
# @data 2016-8-7 下午9:48:39 @versions 1.0
############################################################

TomcatPath = $1
AppName    = $2

# 定义要监控的页面地址  
WebUrl=http://localhost:8080/LuxorCMS

# 获取tomcat进程ID
TomcatID=$(ps -ef |grep tomcat |grep $AppName |grep -v 'grep'|awk '{print $2}')  
  
# tomcat启动程序(这里注意tomcat实际安装的路径)  
StartTomcat = $TomcatPath/bin/startup.sh  
TomcatCache = $TomcatPath/work/Catalina/localhost/$AppName 
  
# 日志输出  
GetPageInfo=/dev/null  
TomcatMonitorLog=$TomcatPath/logs/catalina.log  
  
Monitor()  
{  
  echo "[info] [$(date +'%F %H:%M:%S')] 开始监控tomcat..."  
  
  # 检测Tomcat进程是否存在
  if [[ $TomcatID ]];then
    echo "[info]当前tomcat进程ID为:$TomcatID,继续检测页面..."  
    
    # 检测应用是否启动成功(成功的话页面会返回状态"200")  
    TomcatServiceCode = $(curl -s -o $GetPageInfo -m 10 --connect-timeout 10 $WebUrl -w %{http_code})  
    if [ $TomcatServiceCode -eq 200 ];then  
        echo "[info]页面返回码为$TomcatServiceCode,tomcat启动成功,测试页面正常......"  
    else  
        echo "[error]tomcat页面出错,请注意......状态码为$TomcatServiceCode,错误日志已输出到$GetPageInfo"  
        echo "[error]页面访问出错,开始重启tomcat"  
        kill -9 $TomcatID
        sleep 3  
        rm -rf $TomcatCache
        $StartTomcat  
    fi  
  else  
    echo "[error]tomcat进程不存在!tomcat开始自动重启..."  
    echo "[info]$StartTomcat,请稍候......"  
    rm -rf $TomcatCache  
    $StartTomcat  
  fi  
  echo "------------------------------"  
}
  
Monitor>>$TomcatMonitorLog