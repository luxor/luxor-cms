package com.luxor.cms.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.luxor.cms.entity.Folder;
import com.luxor.cms.repository.FolderDao;
import com.luxor.core.repository.BaseRepository;
import com.luxor.core.service.BaseService;

@Service
public class FolderService extends BaseService<Folder, Integer>
{
	@Resource
	FolderDao folderDao;

	@Override
	protected BaseRepository<Folder, Integer> getDao() {
		return folderDao;
	}	
	
}
