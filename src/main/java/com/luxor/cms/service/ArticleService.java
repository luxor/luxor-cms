package com.luxor.cms.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.luxor.cms.entity.Article;
import com.luxor.cms.repository.ArticleDao;
import com.luxor.core.repository.BaseRepository;
import com.luxor.core.service.BaseService;

@Service
public class ArticleService extends BaseService<Article, Integer>
{
	@Resource
	private ArticleDao articleDao;

	@Override
	protected BaseRepository<Article, Integer> getDao()
	{
		return articleDao;
	}

}
