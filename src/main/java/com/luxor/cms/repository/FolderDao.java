package com.luxor.cms.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.luxor.cms.entity.Folder;
import com.luxor.core.repository.BaseRepository;

@Repository
@Transactional(readOnly = true)
public interface FolderDao extends BaseRepository<Folder, Integer>
{

}
