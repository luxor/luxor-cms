package com.luxor.cms.constant;

/**
 * 主题类型枚举
 * 
 * @author Administrator
 *
 */
public enum ThemeConstant {
	ARTICLE(0, "文章"), IMAGE(1, "图片"), SHOP(2, "商品"), GUESTBOOK(3, "留言板"), RECORD(4, "版本日志"), CUSTOM(5, "自定义");

	private final int index;
	public final String name;

	public static String getName(int index) {
		for (ThemeConstant element : ThemeConstant.values()) {
			if (element.index == index) {
				return element.name;
			}
		}
		return null;
	}

	/**
	 * 枚举构造器
	 * 
	 * @param index
	 * @param name
	 */
	private ThemeConstant(int index, String name) {
		this.index = index;
		this.name = name;
	}

	public int getIndex() {
		return index;
	}

	public String getName() {
		return name;
	}

}