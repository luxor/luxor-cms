package com.luxor.cms.constant;

/**
 * 系统常量
 * 
 * @author XinmingYan @data 2016-4-10 下午6:13:05 
 */
public class SystemConstant {

	/**
	 * 应用部署路径的KEY
	 */
	public static String LUXOR_CMS_ROOT = "luxro.cms.root";

	/**
	 * 上传文件夹
	 */
	public static String UPLOAD_FOLDER = "upload/photo";

	/**
	 * 备份文件夹
	 */
	public static String BACKUP_FOLDER = "/WEB-INF/backup";

	/**
	 * Session中的管理员Key
	 */
	public static final String SESSION_ADMIN = "SESSION_ADMIN";

	/**
	 * Session中的用户Key
	 */
	public static final String SESSION_USER = "SESSION_USER";

}
