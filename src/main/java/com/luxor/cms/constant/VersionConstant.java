package com.luxor.cms.constant;

/**
 * 版本更新类型
 * 
 * @author XinmingYan @dataTime 2017年10月18日 下午7:21:18
 *
 */
public enum VersionConstant {

	ADD(0, "新增"), UPDATE(1, "修复"), SEO(2, "优化"), DELETE(3, "删除");

	private final int index;
	public final String name;

	public static String getName(int index) {
		for (VersionConstant element : VersionConstant.values()) {
			if (element.index == index) {
				return element.name;
			}
		}
		return null;
	}

	/**
	 * 枚举构造器
	 * 
	 * @param index
	 * @param name
	 */
	private VersionConstant(int index, String name) {
		this.index = index;
		this.name = name;
	}

	public int getIndex() {
		return index;
	}

	public String getName() {
		return name;
	}

}
