package com.luxor.cms.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.luxor.cms.entity.Article;
import com.luxor.cms.service.ArticleService;
import com.luxor.core.service.BaseService;
import com.luxor.core.web.AbstractCurdAction;

/**
 * 进入某个内容
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/article")
public class ArticleAction extends AbstractCurdAction<Article, Integer>
{
	@Autowired
	private ArticleService articleService;

	@RequestMapping(value = "/{articleId}.html", method = RequestMethod.GET)
	public String article(@PathVariable("articleId") int id, ModelMap modelMap) {
		return themeService.getThemeUri("article");
	}
		
	@Override
	protected BaseService<Article, Integer> getService() {
		return articleService;
	}	

	@Override
	protected String getContextView() {
		return themeService.getThemeUri("article");
	}
}
