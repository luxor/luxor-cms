package com.luxor.cms.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.luxor.cms.entity.Folder;
import com.luxor.cms.service.FolderService;
import com.luxor.core.service.BaseService;
import com.luxor.core.web.AbstractCurdAction;

/**
 * 进入某个目录
 * 
 * @author XinmingYan @time 2018年2月5日 下午4:19:30
 */
@Controller
@RequestMapping("/folder")
public class FolderAction extends AbstractCurdAction<Folder, Integer>
{
	@Autowired
	FolderService folderService;	
	
	@RequestMapping(value = "/{folderId}.html", method = RequestMethod.GET)
	public String folder(@PathVariable("folderId") long id, ModelMap modelMap)
	{
		return themeService.getThemeUri("folder");
	}

	@Override
	protected BaseService<Folder, Integer> getService()
	{
		return folderService;
	}

	@Override
	protected String getContextView()
	{
		return themeService.getThemeUri("folder");
	}
}
