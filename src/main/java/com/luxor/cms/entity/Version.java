package com.luxor.cms.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.luxor.core.entity.BaseEntity;

/**
 * 系统版本日志
 * 
 * @author Administrator
 *
 */
@Entity
@Table(name = "version")
public class Version extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "folder_id")
	private Folder folder;
	@Column(name = "create_time")
	private Date createTime;
	@Column(name = "title")
	private String title;
	@Column(name = "type")
	private int type;// 1:新增 2:修复 3:优化 4:删除
	@Column(name = "content")
	private String content;

	public Version() {
	}

	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
