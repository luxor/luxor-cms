package com.luxor.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.luxor.core.constant.Active;
import com.luxor.core.entity.BaseEntity;

/**
 * 广告图片资源
 * 
 * @author Administrator
 *
 */
@Entity
@Table(name = "advert")
public class Advert extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "folder_id")
	private Folder folder;// 文件夹
	@Column(name = "title")
	private String title;// 图片标题
	@Column(name = "sort")
	private int sort;// 优先级
	@Column(name = "uri")
	private String uri;// 地址
	@Column(name = "width")
	private int width;// 宽度
	@Column(name = "high")
	private int high;// 高度
	@Column(name = "isBanner")
	private int isBanner;// 是否为横幅图片( 0:非横幅 1:是横幅)
	@Column(name = "status")
	private int status;// 状态(0:隐藏 1:显示)

	public Advert() {
	}

	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHigh() {
		return high;
	}

	public void setHigh(int high) {
		this.high = high;
	}

	public int getIsBanner() {
		return isBanner;
	}

	public void setIsBanner(int isBanner) {
		this.isBanner = isBanner;
	}

	public int getStatus() {
		return status;
	}

	public String getStatusName() {
		return Active.getValue(status);
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
