package com.luxor.cms.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.luxor.core.entity.BaseEntity;
import com.luxor.core.entity.sys.SysUser;

@Entity
@Table(name = "file")
public class File extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;

	@Column(name = "title")
	private String title;

	@Column(name = "uri")
	private String uri;

	@Column(name = "type")
	private String type;

	@Column(name = "content")
	private byte[] content;

	@Column(name = "status")
	private int status;

	@Column(name = "create_time")
	private Date createTime;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private SysUser user;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "aricle_id")
	private Article article;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public SysUser getUser() {
		return user;
	}

	public void setUser(SysUser user) {
		this.user = user;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

}
