package com.luxor.cms.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.luxor.core.constant.Active;
import com.luxor.core.entity.BaseEntity;

/**
 * 文件夹目录
 * 
 * @author Administrator
 *
 */
@Entity
@Table(name = "folder")
public class Folder extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;

	@Column(name = "name")
	private String name;
	@Column(name = "ename")
	private String ename;
	@Column(name = "keywords")
	private String keywords;
	@Column(name = "type")
	private int type;// 0:文章 1：图片 3：商品 4：自定义
	@Column(name = "parent")
	private int parent;
	@Column(name = "path")
	private String path;
	@Column(name = "count")
	private int count;
	@Column(name = "status")
	private int status;// 0:隐藏 1:显示
	@Column(name = "sort")
	private int sort;
	@Column(name = "level")
	private int level;
	@Column(name = "create_time")
	private Date createTime;
	@Column(name = "update_time")
	private Date updateTime;
	@Column(name = "details")
	private String details;
	@Column(name = "content")
	private String content;

	public Folder() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getParent() {
		return parent;
	}

	public void setParent(int parent) {
		this.parent = parent;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getStatus() {
		return status;
	}

	public String getStatusName() {
		return Active.getValue(status);
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
