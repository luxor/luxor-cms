package com.luxor.cms.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.luxor.core.constant.Active;
import com.luxor.core.entity.BaseEntity;
import com.luxor.core.entity.sys.SysUser;

/**
 * 留言板
 * 
 * @author Administrator
 *
 */
@Entity
@Table(name = "guestbook")
public class GuestBook extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private SysUser sysUser;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "folder_id")
	private Folder folder;
	@Column(name = "name")
	private String name;
	@Column(name = "eamil")
	private String eamil;
	@Column(name = "create_time")
	private Date createTime;
	@Column(name = "status")
	private int status;

	public GuestBook() {
	}

	public SysUser getUser() {
		return sysUser;
	}

	public void setUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}

	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEamil() {
		return eamil;
	}

	public void setEamil(String eamil) {
		this.eamil = eamil;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public int getStatus() {
		return status;
	}

	public String getStatusName() {
		return Active.getValue(status);
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
