package com.luxor.cms.entity;

import java.sql.Date;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.luxor.core.constant.Active;
import com.luxor.core.entity.BaseEntity;
import com.luxor.core.entity.sys.SysUser;

/**
 * 文章
 * 
 * @author Administrator
 *
 */
@Entity
@Table(name = "article")
public class Article extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;

	@Column(name = "title")
	private String title;// 标题

	@Basic(fetch = FetchType.LAZY)
	@Column(name = "content")
	private String content;// 内容

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private SysUser user;// 作者编号

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "image_id")
	private Collection<File> files;// 图片附件

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "folder_id")
	private Folder folder;// 所属文件夹

	@Column(name = "path")
	private String path;// 地址

	@Column(name = "view_count")
	private int viewCount;// 浏览数

	@Column(name = "comment_count")
	private int commentCount;// 评论数

	@Column(name = "status")
	private int status;// 状态(0:隐藏 1:显示)

	@Column(name = "create_time")
	private Date createTime;// 创建时间

	@Column(name = "update_time")
	private Date updateTime;// 最后更新时间

	@Column(name = "keywords")
	private String keyWords;// 关键字

	public Article() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public SysUser getAdmin() {
		return user;
	}

	public void setAdmin(SysUser sysUser) {
		this.user = sysUser;
	}

	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getViewCount() {
		return viewCount;
	}

	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}

	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public int getStatus() {
		return status;
	}

	public String getStatusName() {
		return Active.getValue(status);
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}

	public SysUser getUser() {
		return user;
	}

	public void setUser(SysUser user) {
		this.user = user;
	}

	public Collection<File> getImages() {
		return files;
	}

	public void setImages(Collection<File> files) {
		this.files = files;
	}

}
