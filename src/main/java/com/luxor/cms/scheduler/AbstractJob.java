package com.luxor.cms.scheduler;

/**
 * 定时任务抽象类
 * 
 * @autdor XinmingYan @data 2016-8-20 下午4:56:12 @versions 1.0
 * @packagePath com.luxor.core.support.task.Job.java
 */
public abstract class AbstractJob
{
	/**
	 * 定时任务调用的入口方法
	 */
	public abstract void doJob();
}
