package com.luxor.cms.scheduler;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.luxor.core.utils.PropertyUtils;

/**
 * 定时任务：检测系统运行状况,永久保持系统处于运行状态。
 * 
 * @author XinmingYan @data 2016-7-21 上午9:24:05 @versions 1.0
 * @packagePath com.luxor.cms.scheduler.JobShell.java
 */
public class JobShell extends AbstractJob
{
	private static Logger logger = LoggerFactory.getLogger(JobShell.class);

	private static final String TAILED = ";";
	private static final String SPACING = " ";
	private static final String SLASH = "/";

	private String appName = "luxor.cms.node.01";	
	private String tomcatPath = "/use/local/tomcat";
	private String shellPath = "bin/immortal.sh";

	@Override
	public void doJob() {
		logger.info("Start doJob exec Shell : clearoldfiles.sh !");

		// 脚本授权
		String command1 = "chmod u+x " + shellPath + TAILED;
		command(command1);

		// 脚本执行
		String commandPath = "/bin/sh " + PropertyUtils.getRoot() + SLASH + shellPath + SPACING;
		String command2 = commandPath + tomcatPath + SPACING + appName + TAILED;
		command(command2);

		logger.info("End exec Shell : clearoldfiles.sh !");
	}

	/**
	 * Shell 执行函数
	 * 
	 * @param command
	 * @return 0:成功, >0：失败
	 */
	private int command(String command) {
		Process process = null;
		int result = -1;
		try {
			process = Runtime.getRuntime().exec(command);
			result = process.waitFor();
			logger.info("Exec command. result:{} / command:{}", new Object[] { result, command });
			return result;
		} catch (Exception e) {
			logger.error("invoking shell job error! message:{}", e.getMessage());
			throw new RuntimeException("invoking shell job error!");
		} finally {
			IOUtils.closeQuietly(process.getOutputStream());
			IOUtils.closeQuietly(process.getInputStream());
			IOUtils.closeQuietly(process.getErrorStream());
		}
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getTomcatPath() {
		return tomcatPath;
	}

	public void setTomcatPath(String tomcatPath) {
		this.tomcatPath = tomcatPath;
	}

	public String getShellPath() {
		return shellPath;
	}

	public void setShellPath(String shellPath) {
		this.shellPath = shellPath;
	}

}
