package com.luxor.core.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 序列化对象工具类
 * 
 * @auditor XinmingYan @data 2016-8-8 下午12:55:58 @versions 1.0
 * @packagePath com.luxor.core.util.SerializableUtils.java
 */
public class SerializableUtils
{
	private static final Logger logger = LoggerFactory.getLogger(SerializableUtils.class);

	/**
	 * 序列化
	 * @param value
	 * @return
	 */
	public static <T extends Serializable> byte[] serialize(Object value)
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try
		{
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(value);
		} catch (IOException e)
		{
			logger.error("serialize error!", e);
		}
		return bos.toByteArray();
	}

	/**
	 * 反序列化
	 * @param bytes
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> T deSerialize(byte[] bytes)
	{
		ByteArrayInputStream bIs = new ByteArrayInputStream(bytes);
		try
		{
			ObjectInputStream oIs = new ObjectInputStream(bIs);
			return (T) oIs.readObject();
		} catch (Exception e)
		{
			logger.error("deSerialize error!", e);
		}
		return null;
	}
}
