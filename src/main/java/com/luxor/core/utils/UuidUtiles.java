package com.luxor.core.utils;

import java.util.UUID;

public class UuidUtiles
{
	public static final String[] CHARS = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
			"u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
			"L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

	public static void main(String[] args) {
		System.out.println(randomUUID8());
	}

	/**
	 * 获取8位UUID (精度有所损失！)
	 * @return
	 */
	public static String randomUUID8() {
		StringBuffer shortBuffer = new StringBuffer();
		String uuid = randomUUID32();
		for (int i = 0; i < 8; i++) {
			String str = uuid.substring(i * 4, i * 4 + 4);
			int x = Integer.parseInt(str, 16);
			shortBuffer.append(CHARS[x % 0x3E]);
		}
		return shortBuffer.toString();
	}
	
	
	/**
	 * 获取36位UUID(原生UUID)
	 * @return
	 */
	public static String randomUUID() {
		return UUID.randomUUID().toString();
	}

	/**
	 * 获取32位UUID
	 * @return
	 */
	public static String randomUUID32() {
		return UUID.randomUUID().toString().replace("-", "");
	}
	
}
