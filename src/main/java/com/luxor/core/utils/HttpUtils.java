package com.luxor.core.utils;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.luxor.core.entity.ResultVo;

/**
 * HTTP工具类
 * 
 * @author XinmingYan @dataTime 2017年10月22日 下午12:10:17
 */
public class HttpUtils {
	private static Logger logger = LoggerFactory.getLogger(HttpUtils.class);

	// -- header 常量定义 --//
	private static final String DEFAULT_ENCODING = "UTF-8";

	// Content Type 定义
	public static final String TEXT_TYPE = "text/plain";
	public static final String XML_TYPE = "text/xml";
	public static final String HTML_TYPE = "text/html";
	public static final String JS_TYPE = "text/javascript";
	public static final String JSON_TYPE = "application/json";
	public static final String EXCEL_TYPE = "application/vnd.ms-excel";

	/**
	 * 获取请求的根目录
	 * 
	 * @return http://127.0.0.1:8080/LuxroCMS
	 */
	public static String getBasePath(HttpServletRequest request) {
		String path = request.getContextPath();
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
		return basePath;
	}

	/**
	 * 获取客户端的IP
	 * 
	 * @return 127.0.0.1
	 */
	public static String getIp(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		} else if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		} else if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		} else if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		} else if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	/**
	 * 通过request对象判断是否为Ajax请求
	 * 
	 * @param request
	 * @return
	 */
	public static boolean isAjax(HttpServletRequest request) {
		String header = request.getHeader("x-requested-with");

		if (header == null || header.length() == 0 || "unknown".equalsIgnoreCase(header)) {
			return false;
		}

		if ("XMLHttpRequest".equals(header)) {
			return true;
		}
		return false;
	}

	// -- 绕过jsp/freemaker直接输出文本的函数 --//
	/**
	 * 直接输出内容的简便函数.
	 * 
	 * eg. send("text/plain", "hello"); send("text/html","hello"); send("text/xml",
	 * "hello"); ...
	 */
	public static void send(HttpServletResponse response, final String contentType, final String content) {
		response.setCharacterEncoding(DEFAULT_ENCODING);
		String fullContentType = contentType + ";charset=" + DEFAULT_ENCODING;
		response.setContentType(fullContentType);
		PrintWriter out = null;
		try {
			response.reset();
			out = response.getWriter();
			out.write(content);
			out.flush();
		} catch (Exception e) {
			logger.error("Error The HttpUtils.send().", e);
		} finally {
			out.close();
		}
	}

	/**
	 * 直接输出文本.
	 * 
	 * @see #send(String, String, String...)
	 */
	public static void sendText(HttpServletResponse response, final String text) {
		send(response, TEXT_TYPE, text);
	}

	/**
	 * 直接输出HTML.
	 * 
	 * @see #send(String, String, String...)
	 */
	public static void sendHtml(HttpServletResponse response, final String html) {
		send(response, HTML_TYPE, html);
	}

	/**
	 * 直接输出XML.
	 */
	public static void sendXml(HttpServletResponse response, final String xml) {
		send(response, XML_TYPE, xml);
	}

	/**
	 * 直接输出JSON,使用Jackson转换Java对象.
	 * 
	 * @param data
	 *            可以是List<POJO>, POJO[], POJO, 也可以Map名值对.
	 * @see #send(String, String, String...)
	 */
	public static void sendJson(HttpServletResponse response, final ResultVo data) {
		send(response, JSON_TYPE, JSON.toJSONString(data));
	}

}
