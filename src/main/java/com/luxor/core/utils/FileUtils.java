package com.luxor.core.utils;

import java.io.File;
import java.io.IOException;

/**
 * 文件操作工具类
 * 
 * @author XinmingYan @data 2016-4-30 下午6:00:45 @versions 1.0
 * @packagePath com.luxro.cms.util.FileUtils.java
 */
public class FileUtils {
	/**
	 * 检查文件或文件夹是否存在，不存在就创建
	 * 
	 * @param file new File("/20160829/115730/logo.png")
	 * @return boolean
	 * @throws IOException
	 */
	public static boolean checkFile(File file) throws IOException {
		if (file.exists()) {
			return true;
		}
		boolean b1 = file.getParentFile().mkdirs();
		boolean b2 = file.createNewFile();
		return b1 && b2;
	}

	/**
	 * 删除文件/文件夹
	 * 
	 * @param path 要删除的文件/文件夹
	 * @return
	 */
	public static boolean deleteAllFilesOfDir(File path) {
		if (!path.exists()) {
			return false;
		}
		if (path.isFile()) {
			return path.delete();
		}
		File[] files = path.listFiles();
		for (int i = 0; i < files.length; i++) {
			deleteAllFilesOfDir(files[i]);
		}
		return path.delete();
	}

	/**
	 * 判断文件夹是否存在，如果不存在就创建
	 * 
	 * @param path "C:/20160829/115730"
	 */
	public static void existsFolder(String path) {
		File file = new File(path);
		if (!file.isDirectory()) {
			file.mkdir();
		}
	}

}
