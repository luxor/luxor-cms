package com.luxor.core.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.UnknownAlgorithmException;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MD5加密解密
 * 
 * @author XinmingYan @data 2016-4-10 下午4:25:14 @versions 1.0
 * @packagePath com.luxro.cms.util.AuthUtils.java
 */
public class MD5Utils
{
	private static Logger logger = LoggerFactory.getLogger(MD5Utils.class);

	private static final String ALGORITHM_NAME = "MD5";

	private static final String CHARSET= "UTF-8";

	public static void main(String[] args)
	{
		String password = MD5Utils.shiroDigest("123456", "25055ce0-cecd-4e10-8ff0-f1d0c674e8f5");
		System.out.println(password);
	}

	/**
	 * 普通MD5加密
	 * @param str
	 * @return
	 */
	public static synchronized String digest(String resource)
	{
		if (StringUtils.isEmpty(resource))
		{
			logger.error("Not an MD5 hash of '{}', because param is empty！", resource);
			return null;
		}
		MessageDigest digest = getDigest(ALGORITHM_NAME);
		byte[] bytes = null;
		try
		{
			bytes = digest.digest(resource.getBytes(CHARSET));
		} catch (UnsupportedEncodingException e)
		{
			logger.error("Error: String to Bytes.", e);
		}
		// 将加密后的byte数组转换为十六进制的字符串,否则的话生成的字符串会乱码
		StringBuffer ret = new StringBuffer(bytes.length << 1);
		for (int i = 0; i < bytes.length; i++)
		{
			ret.append(Character.forDigit((bytes[i] >> 4) & 0xf, 16));
			ret.append(Character.forDigit(bytes[i] & 0xf, 16));
		}
		return ret.toString();
	}

	/**
	 * 基于Shiro的密码加密<不可删除>
	 * @param resource
	 * @return
	 */
	public static String shiroDigest(String resource, String salt)
	{
		ByteSource s = ByteSource.Util.bytes(salt);
		SimpleHash hash = new SimpleHash(ALGORITHM_NAME, resource, s, 1);
		return hash.toString();
	}

	public static MessageDigest getDigest(String algorithmName) throws UnknownAlgorithmException
	{
		MessageDigest messageDigest = null;
		try
		{
			messageDigest = MessageDigest.getInstance(algorithmName);
			messageDigest.reset();
			return messageDigest;
		} catch (NoSuchAlgorithmException e)
		{
			logger.error("No native '{}' MessageDigest instance available on the current JVM.", algorithmName);
			throw new UnknownAlgorithmException(e);
		}
	}

	public static synchronized String randomUUID()
	{
		UUID uuid = UUID.randomUUID();
		String str = uuid.toString();
		return str;
	}
}
