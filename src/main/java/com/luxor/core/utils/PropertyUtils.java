package com.luxor.core.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import com.luxor.core.exception.sys.ParamNullException;

public class PropertyUtils extends PropertyPlaceholderConfigurer
{
	private final static Logger logger = LoggerFactory.getLogger(PropertyUtils.class);

	private static Map<String, String> propertys = new HashMap<String, String>();

	@Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess, Properties props) throws BeansException {
		super.processProperties(beanFactoryToProcess, props);
		for (Object key : props.keySet()) {
			String keyStr = key.toString();
			String value = props.getProperty(keyStr);
			propertys.put(keyStr, value);
		}
	}

	public static String get(String key) {
		String value = propertys.get(key);
		if (StringUtils.isBlank(value)) {
			logger.warn("PropertyUtils get key:{} ,value: IS NULL", key);
			throw new ParamNullException("PropertyUtils.key is null!");
		}
		return value;
	}

	public static String get(String key, String defaultValue) {
		String value = propertys.get(key);
		if (StringUtils.isBlank(value)) {
			return defaultValue;
		} else {
			return value;
		}
	}

	public static Integer getInt(String key) {
		String value = get(key);
		return Integer.parseInt(value);
	}

	public static String getRoot() {
		String rootKey = "luxro.cms.root";
		String cmsRoot = System.getProperty(rootKey);
		logger.debug("PropertyUtils get key:root ,value:{}", cmsRoot);
		return cmsRoot;
	}
}
