package com.luxor.core.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
	// 默认的时间格式
	public static final String FORMATE_TIMESTANP = "yyyy-MM-dd HH:mm:ss";

	// 默认的日期格式
	public static final String FORMATE_DATE = "yyyy-MM-dd";

	public static final ThreadLocal<DateFormat> DEFAULT_FORMATE_TIMESTANP = new ThreadLocal<DateFormat>() {
		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat(FORMATE_TIMESTANP);
		}
	};

	public static String getCurrtime() {
		return DEFAULT_FORMATE_TIMESTANP.get().format(new Date(System.currentTimeMillis()));
	}

	/**
	 * 返回当前时间的串
	 * 
	 * @param fmt 时间格式,如"yyyy-MM-dd HH:mm:ss"
	 * @return 字符串："2016-09-01 21:11:32"
	 */
	public static String getCurrtime(String fmt) {
		SimpleDateFormat format = new SimpleDateFormat(fmt);
		return format.format(new Date(System.currentTimeMillis()));
	}

	/**
	 * 将字符串转换为时间对象
	 * 
	 * @param strDate "2008-07-10 19:20:00"
	 * @param fmt 时间格式,如"yyyy-MM-dd HH:mm:ss"
	 * @return
	 */
	public static Date stringToDate(String strDate, String fmt) {
		try {
			SimpleDateFormat format = new SimpleDateFormat(fmt);
			if (strDate != null && strDate.length() > 0) {
				return format.parse(strDate);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Date stringToDate(String strDate) {
		try {
			if (strDate != null && strDate.length() > 0) {
				return DEFAULT_FORMATE_TIMESTANP.get().parse(strDate);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String dateToString(Date dateTime) {
		if (dateTime == null) {
			return null;
		}
		return DEFAULT_FORMATE_TIMESTANP.get().format(dateTime);
	}
}
