package com.luxor.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 控制层参数中,获取当前登陆的用户
 * 
 * @auditor XinmingYan @data 2016-8-25 上午11:36:23 @versions 1.0
 * @packagePath com.luxor.core.annotation.Principal.java
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Principal
{
	// 指定参数名称
	String value() default "";
}
