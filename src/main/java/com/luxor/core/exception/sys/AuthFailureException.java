package com.luxor.core.exception.sys;

import com.luxor.core.exception.BaseException;

/**
 * 用户认证失效
 * 
 * 出现这种异常,代表需要重新登陆
 * 
 * @auditor XinmingYan @time 2017年8月27日 下午6:25:04 @versions 0.0.1
 * @packagePath com.luxor.core.exception.sys.AuthFailureException.java
 */
public class AuthFailureException extends BaseException {

	private static final long serialVersionUID = 1L;

	public AuthFailureException() {
		super();
	}

	public AuthFailureException(String message, Throwable cause) {
		super(message, cause);
	}

	public AuthFailureException(String message) {
		super(message);
	}

	public AuthFailureException(Throwable cause) {
		super(cause);
	}

}
