package com.luxor.core.exception.sys;

import com.luxor.core.exception.BaseException;

/**
 * 服务异常
 * 
 * @auditor XinmingYan @data 2016-9-9 下午2:01:22 @versions 1.0
 * @packagePath com.luxor.core.exception.sys.ServiceException.java
 */
public class ServiceException extends BaseException
{
	private static final long serialVersionUID = 1L;

	public ServiceException()
	{
		super();
	}

	public ServiceException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public ServiceException(String message)
	{
		super(message);
	}

	public ServiceException(Throwable cause)
	{
		super(cause);
	}

}
