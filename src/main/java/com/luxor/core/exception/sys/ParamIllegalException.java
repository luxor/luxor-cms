package com.luxor.core.exception.sys;

import com.luxor.core.exception.BaseException;

/**
 * 参数错误
 * 
 * @auditor XinmingYan @data 2016-8-25 上午11:47:24 @versions 1.0
 * @packagePath com.luxor.core.exception.sys.ParamValidException.java
 */
public class ParamIllegalException extends BaseException
{
	private static final long serialVersionUID = 1L;

	public ParamIllegalException()
	{
		super();
	}

	public ParamIllegalException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public ParamIllegalException(String message)
	{
		super(message);
	}

	public ParamIllegalException(Throwable cause)
	{
		super(cause);
	}

}
