package com.luxor.core.exception.sys;

import com.luxor.core.exception.BaseException;

/**
 * 资源不存在
 * 
 * @author XinmingYan @time 2017年11月8日 下午4:48:47
 */
public class NotFoundException extends BaseException{

	private static final long serialVersionUID = 1L;

	public NotFoundException() {
		super();
	}

	public NotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotFoundException(String message) {
		super(message);
	}

	public NotFoundException(Throwable cause) {
		super(cause);
	}

}
