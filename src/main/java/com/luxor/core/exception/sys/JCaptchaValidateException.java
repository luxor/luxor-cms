package com.luxor.core.exception.sys;

import org.apache.shiro.authc.AuthenticationException;

/**
 * 验证码错误
 * 
 * @auditor XinmingYan @data 2016-8-23 下午2:14:21 @versions 1.0
 * @packagePath com.luxor.core.exception.JCaptchaValidateException.java
 */
public class JCaptchaValidateException extends AuthenticationException
{
	private static final long serialVersionUID = 1L;

	public JCaptchaValidateException()
	{
		super();
	}

	public JCaptchaValidateException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public JCaptchaValidateException(String message)
	{
		super(message);
	}

	public JCaptchaValidateException(Throwable cause)
	{
		super(cause);
	}

}
