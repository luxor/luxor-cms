package com.luxor.core.exception.sys;

import com.luxor.core.exception.BaseException;

/**
 * 禁止删除
 * 
 * @auditor XinmingYan @data 2016-8-24 下午12:17:00 @versions 1.0
 * @packagePath com.luxor.core.exception.sys.NotDeleteException.java
 */
public class NotDeleteException extends BaseException
{
	private static final long serialVersionUID = 1L;

	public NotDeleteException()
	{
		super();
	}

	public NotDeleteException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public NotDeleteException(String message)
	{
		super(message);
	}

	public NotDeleteException(Throwable cause)
	{
		super(cause);
	}

}
