package com.luxor.core.exception.sys;


/**
 * 参数不能为空
 * 
 * @auditor XinmingYan @data 2016-8-15 上午9:22:35 @versions 1.0
 * @packagePath com.luxor.core.exception.ParameterNullException.java
 */
public class ParamNullException extends ParamIllegalException
{
	private static final long serialVersionUID = 1L;

	public ParamNullException()
	{
		super();
	}

	public ParamNullException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public ParamNullException(String message)
	{
		super(message);
	}

	public ParamNullException(Throwable cause)
	{
		super(cause);
	}

}
