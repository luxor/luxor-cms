package com.luxor.core.exception;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.ShiroException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import com.luxor.core.constant.Constants;
import com.luxor.core.entity.ResultVo;
import com.luxor.core.service.ThemeService;
import com.luxor.core.service.sys.SysConfigService;
import com.luxor.core.utils.HttpUtils;

/**
 * SpringMVC 异常处理器
 * 
 * @auditor XinmingYan @data 2016-7-22 下午3:57:37 @versions 1.0
 * @packagePath com.luxro.core.exception.MyExceptionHandler.java
 */
public class ExceptionHandler implements HandlerExceptionResolver {
	private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);
	@Autowired
	private ThemeService themeService;
	@Autowired
	private SysConfigService sysConfigService;

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		if (logger.isErrorEnabled()) {
			logger.error("Error:{}", ex);
		}
		Map<String, Object> model = new HashMap<String, Object>();
		if (HttpUtils.isAjax(request)) {
			// Ajax请求过程中产生的异常处理
			HttpUtils.sendJson(response, new ResultVo(false, ex.getMessage()));
			return new ModelAndView();
		} else {
			// 模板名称
			String skin = sysConfigService.getTheme();
			String basePath = HttpUtils.getBasePath(request);
			model.put(Constants.BASE_PATH, basePath);
			model.put(Constants.BASE_PLUGINS_PATH, basePath + "/static/plugins");
			model.put(Constants.BASE_MANAGE_PATH, basePath + "/static/skin/manage/");
			model.put(Constants.BASE_TEMPLATE_PATH, basePath + "/static/skin/template/" + skin);
			if (ex instanceof ShiroException) {
				// 权限异常的处理
				return new ModelAndView(themeService.getLogin(), model);
			}
			// 普通异常的处理
			return new ModelAndView(themeService.getError(), model);
		}
	}

}
