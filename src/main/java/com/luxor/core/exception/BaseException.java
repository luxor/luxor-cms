package com.luxor.core.exception;

/**
 * 自定义异常
 * 
 * @auditor XinmingYan @data 2016-7-8 上午11:31:29 @versions 1.0
 * @packagePath com.luxro.cms.exception.AuthenException.java
 */
public class BaseException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	public BaseException()
	{
		super();
	}

	public BaseException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public BaseException(String message)
	{
		super(message);
	}

	public BaseException(Throwable cause)
	{
		super(cause);
	}

}