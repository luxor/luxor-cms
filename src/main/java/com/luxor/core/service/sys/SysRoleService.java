package com.luxor.core.service.sys;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.luxor.core.entity.sys.SysRole;
import com.luxor.core.repository.sys.SysRoleDao;
import com.luxor.core.service.BaseService;

@Service
public class SysRoleService extends BaseService<SysRole, Integer> {
	@Resource
	private SysRoleDao sysRoleDao;

	@Override
	protected SysRoleDao getDao() {
		return sysRoleDao;
	}

	}
