package com.luxor.core.service.sys;

import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.springframework.stereotype.Service;

/**
 * 在线用户会话服务
 * 
 * @auditor XinmingYan @data 2016-8-25 下午2:54:07 @versions 1.0
 * @packagePath com.luxor.core.service.sys.SysSessionService.java
 */
@Service
public class SysSessionService
{
	@Resource(name = "sessionDao")
	private SessionDAO sessionDao;

	/**
	 * 踢出某个在线用户
	 * @param username
	 */
	public void kickOut(String username)
	{
		if (StringUtils.isNotEmpty(username))
		{
			return;
		}

		Collection<Session> sessions = sessionDao.getActiveSessions();
		for (Session session : sessions)
		{
			String name = (String) session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
			if (username.equals(name))
			{
				session.setTimeout(0);// 设置session立即失效，即将其踢出系统
				sessionDao.delete(session);
				break;
			}
		}
	}
}
