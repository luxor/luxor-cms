package com.luxor.core.service.sys;

import java.sql.Date;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authc.UnknownAccountException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.luxor.core.entity.sys.SysUser;
import com.luxor.core.repository.sys.SysUserDao;
import com.luxor.core.service.BaseService;
import com.luxor.core.utils.MD5Utils;

/**
 * 用户管理服务
 * 
 * @author XinmingYan @dataTime 2017年10月22日 上午11:51:04
 */
@Service
public class SysUserService extends BaseService<SysUser, Integer> {
	private static Logger logger = LoggerFactory.getLogger(SysUserService.class);

	@Resource
	private SysUserDao sysUserDao;


	@Override
	protected SysUserDao getDao() {
		return sysUserDao;
	}

	/**
	 * 根据用户名获取用户
	 * 
	 * @param username
	 * @return
	 */
	public SysUser findByUsername(String username) {
		if (StringUtils.isEmpty(username)) {
			logger.warn("findByUsername error, username is empty!");
			return null;
		}
		SysUser user = sysUserDao.findByUsername(username);
		if (user == null) {
			throw new UnknownAccountException("This user is not exists!");
		}
		return user;
	}

	/**
	 * 更新最后登陆时间
	 * 
	 * @param user
	 */
	public void refreshLastTime(Integer userId) {
		if (userId != null) {
			SysUser user = sysUserDao.getOne(userId);
			user.setLastTime(new Date(System.currentTimeMillis()));
			sysUserDao.save(user);
		}
	}

	/**
	 * 创建新的注册用户
	 * 
	 * @param user
	 * @return
	 */
	public boolean createUser(SysUser user) {
		if (user == null || user.getPassword() == null || user.getUsername() == null) {
			logger.warn("createUser error, user is empty!");
			return false;
		}
		String password = user.getPassword();
		String salt = MD5Utils.randomUUID();
		String solt = user.getSalt();
		String passwordMd5 = MD5Utils.shiroDigest(password, solt);
		user.setSalt(salt);
		user.setPassword(passwordMd5);
		save(user);
		return true;
	}

	/**
	 * 修改密码
	 * 
	 * @param username
	 * @param oldPassword
	 * @param newPassword
	 * @return
	 */
	public boolean changePassword(String username, String oldPassword, String newPassword) {
		SysUser user = sysUserDao.findByUsernameAndStatus(username, 1);
		if (user == null) {
			logger.warn("changePassword user:{} not exist!", username);
			return false;
		}
		String solt = user.getSalt();
		String oldPasswordMd5 = MD5Utils.shiroDigest(oldPassword, solt);
		if (StringUtils.equals(user.getPassword(), oldPasswordMd5)) {
			String password = MD5Utils.digest(newPassword);
			user.setPassword(password);
			save(user);
			return true;
		}
		return false;
	}

}
