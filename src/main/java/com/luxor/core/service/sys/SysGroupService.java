package com.luxor.core.service.sys;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.luxor.core.entity.sys.SysGroup;
import com.luxor.core.repository.sys.SysGroupDao;
import com.luxor.core.service.BaseService;

@Service
public class SysGroupService extends BaseService<SysGroup, Integer> {
	@Resource
	private SysGroupDao sysGroupDao;

	@Override
	protected SysGroupDao getDao() {
		return sysGroupDao;
	}

}
