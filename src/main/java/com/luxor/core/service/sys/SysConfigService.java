package com.luxor.core.service.sys;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.luxor.core.entity.sys.SysConfig;
import com.luxor.core.repository.sys.SysConfigDao;
import com.luxor.core.service.BaseService;

@Service
public class SysConfigService extends BaseService<SysConfig, Integer> {
	@Resource
	private SysConfigDao sysConfigDao;

	@Override
	protected SysConfigDao getDao() {
		return sysConfigDao;
	}

	/**
	 * 获取模板名称
	 */
	public String getTheme() {
		return findByKey(SysConfig.LUXOR_THEME);
	}

	public int getIntByKey(String key) {
		String value = findByKey(key);
		int result = StringUtils.isBlank(value) ? 0 : Integer.parseInt(value);
		return result;
	}

	@Cacheable(value = "sysConfig", key = "#key")
	public String findByKey(String key) {
		SysConfig sysConfig = getDao().getByKey(key);
		String result = sysConfig == null ? "" : sysConfig.getValue();
		return result;
	}

	/**
	 * 保存配置
	 * 
	 * @param key
	 * @param value
	 */
	@Override
	@CachePut(value = "sysConfig", key = "#key")
	public void save(SysConfig config) {
		getDao().save(config);
	}

	/**
	 * 删除配置
	 * 
	 * @param key
	 * @return Integer
	 */
	public void deleteByKey(String key) {
		SysConfig entity = getDao().getByKey(key);
		delete(entity);
	}

	@Override
	@CacheEvict(value = "sysConfig", key = "#entity.key")
	public void delete(SysConfig entity) {
		if (entity == null) {
			return;
		}
		if (entity.getCanDel() == 1) {
			getDao().delete(entity);
		}
	}
}