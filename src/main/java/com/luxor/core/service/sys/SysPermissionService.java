package com.luxor.core.service.sys;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.luxor.core.entity.sys.SysPermission;
import com.luxor.core.repository.sys.SysPermissionDao;
import com.luxor.core.service.BaseService;

@Service
public class SysPermissionService extends BaseService<SysPermission, Integer> {
	@Resource
	private SysPermissionDao sysPerissionDao;

	@Override
	protected SysPermissionDao getDao() {
		return sysPerissionDao;
	}

	public SysPermission findByUrl(String uri) {
		return sysPerissionDao.findByUriAndStatus(uri, 1);
	}

	@Override
	public void delete(Integer id) {
		delete(getEntity(id));
	}

	@Override
	public void delete(SysPermission perission) {
		getDao().delete(perission);
	}
}
