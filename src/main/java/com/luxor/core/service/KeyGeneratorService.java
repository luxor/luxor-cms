package com.luxor.core.service;

import java.lang.reflect.Method;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.stereotype.Service;

/**
 * key-value 缓存中,key的生成策略
 * 
 * @author XinmingYan @data 2016-4-10 下午7:34:46 @versions 1.0
 * @packagePath com.luxro.cms.service.KeyGeneratorService.java
 */
@Service
public class KeyGeneratorService implements KeyGenerator
{
	private Logger log = LoggerFactory.getLogger(KeyGeneratorService.class);

	/**
	 * key的生成策略
	 * @return ClassName_methodName_param1_param2_param3...
	 */
	@Override
	public Object generate(Object target, Method method, Object... params)
	{
		String className = target.getClass().getSimpleName();
		String methodName = method.getName().toLowerCase();
		String key = className + "_" + methodName + "_" + StringUtils.join(params, "_");
		log.debug("KEY:" + key);
		return new SimpleKey(key);
	}
}
