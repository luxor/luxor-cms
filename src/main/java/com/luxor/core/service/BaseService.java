package com.luxor.core.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

import com.luxor.core.entity.DataTableVo;
import com.luxor.core.plugin.search.QueryParam;
import com.luxor.core.repository.BaseRepository;

/**
 * Service 基类
 * 
 * @auditor XinmingYan @data 2016-7-7 下午6:13:27 @versions 1.0
 * @packagePath com.luxro.cms.service.BaseService.java
 */
public abstract class BaseService<T, PK extends Serializable>
{
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	protected abstract BaseRepository<T, PK> getDao();

	/**
	 * 返回 <Bootstrap Table插件> 需要的JSON数据
	 * @param pageable 分页参数
	 * @param filters 过滤条件
	 * @return
	 */
	public DataTableVo<T> findDataTable(final Pageable pageable, final List<QueryParam> filters)
	{
		Page<T> pages = getDao().findAll(pageable, filters);
		long total = pages.getTotalElements();
		List<T> rows = pages.getContent();
		return new DataTableVo<T>(total, rows);
	}

	/**
	 * 动态sql查询
	 * @param filters
	 * @return
	 */
	public Collection<T> findAll(List<QueryParam> filters)
	{
		Collection<T> list = getDao().findAll(filters);
		return list;
	}

	/**
	 * 动态sql分页查询
	 * @param filters
	 * @return
	 */
	public Page<T> findAll(Pageable pageable, List<QueryParam> filters)
	{
		Page<T> list = getDao().findAll(pageable, filters);
		return list;
	}

	/**
	 * 保存
	 * */
	public void save(@Validated T entity)
	{
		getDao().save(entity);
	}

	/**
	 * 批量保存实体
	 * */
	public void save(Collection<T> entitys)
	{
		for (T entity : entitys) {
			save(entity);
		}
	}

	/**
	 * 删除
	 */
	public void delete(PK id)
	{
		getDao().delete(id);
	}

	/**
	 * 批量删除
	 * 
	 * @param ids
	 */
	public void delete(PK ids[])
	{
		for (PK id : ids) {
			delete(id);
		}
	}

	/**
	 * 删除
	 * 
	 */
	public void delete(T entity)
	{
		getDao().delete(entity);
	}

	/**
	 * 批量删除
	 */
	public void delete(Collection<T> entitys)
	{
		for (T entity : entitys) {
			delete(entity);
		}
	}

	/**
	 * 查询单个对象
	 * 
	 * @param id 实体主键
	 * @return
	 */
	public T getEntity(PK id)
	{
		return getDao().findOne(id);
	}

	/**
	 * 获取所有实体
	 * 
	 * @return
	 */
	public List<T> findAll()
	{
		return getDao().findAll();
	}

	/**
	 * 判断实体是否存在
	 * 
	 * @param idValue 实体的主键ID
	 * @return
	 */
	public boolean exists(PK idValue)
	{
		return getDao().exists(idValue);
	}

}
