package com.luxor.core.service;

import java.io.File;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.luxor.core.service.sys.SysConfigService;
import com.luxor.core.utils.PropertyUtils;

/**
 * 模板服务类
 * 
 * @auditor XinmingYan @data 2016-7-7 上午9:20:02 @versions 1.0
 * @packagePath com.luxro.cms.service.ThemeService.java
 */
@Service
public class ThemeService {
	private static final String UPLOAD_PATH = "upload";

	@Resource
	private SysConfigService sysConfigService;

	public String getManageUri(String uri) {
		return "/manage/" + uri;
	}

	public String getThemeUri(String uri) {
		return "/template/" + sysConfigService.getTheme() + "/" + uri;
	}

	/**
	 * 后台首页(默认)模板
	 * 
	 * @return
	 */
	public String getManageIndex() {
		return getManageUri("index");
	}

	/**
	 * 首页(默认)模板
	 * 
	 * @return
	 */
	public String getIndex() {
		return getThemeUri("index");
	}

	/**
	 * 登陆页面
	 * 
	 * @return
	 */
	public String getLogin() {
		return getManageUri("security/login");
	}

	/**
	 * 上传文件存储目录
	 * 
	 * @return
	 */
	public String getUpload() {
		return PropertyUtils.getRoot() + File.separator + UPLOAD_PATH;
	}

	/**
	 * 重定向
	 * 
	 * @return
	 */
	public String getRedirect(String uri) {
		return "redirect:/" + uri;
	}

	public String getError() {
		return getThemeUri("error");
	}

	public String get404() {
		return getThemeUri("404");
	}

	public String get405() {
		return getThemeUri("405");
	}

}
