package com.luxor.core.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Bootstrap Table 表格插件
 * JSON数据模型
 * @auditor XinmingYan @data 2016-8-29 下午4:40:15 @versions 1.0
 * @packagePath com.luxor.core.entity.TablesVo.java
 */
public class DataTableVo<T>
{
	// 返回的总数量
	private long total = 0;

	// 返回的列表
	private List<T> rows = new ArrayList<T>();

	public DataTableVo()
	{
		// TODO
	}

	public DataTableVo(Long total, List<T> rows)
	{
		this.total = total;
		this.rows = rows;
	}

	public long getTotal()
	{
		return total;
	}

	public void setTotal(long total)
	{
		this.total = total;
	}

	public List<T> getRows()
	{
		return rows;
	}

	public void setRows(List<T> rows)
	{
		this.rows = rows;
	}
}
