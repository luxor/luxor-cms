package com.luxor.core.entity.sys;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.luxor.core.entity.BaseEntity;

/**
 * 组
 * 
 * @auditor XinmingYan @data 2016-7-28 下午2:56:02 @versions 1.0
 * @packagePath com.luxro.core.entity.sys.SysGroup.java
 */
@Entity
@Table(name = "T_SYS_GROUP")
public class SysGroup extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;
	@NotNull(message = "权限组标识不能为空")
	@Column(name = "`group`", nullable = false, unique = true)
	private String group;// 组代码
	@Size(min = 2, max = 15, message = "权限组名字长度在2到15岁之间")
	@Column(name = "name", unique = true)
	private String name;
	@Column(name = "details")
	private String details;
	@Column(name = "status")
	private int status = 1;// (0-失效;1-正常;2-待审核)
	@Column(name = "sort")
	private int sort;
	@Column(name = "pid")
	private int pid;

	@ManyToMany(mappedBy = "groups")
	private Set<SysUser> users = new HashSet<SysUser>(2);

	@ManyToMany(cascade = { CascadeType.DETACH })
	@JoinTable(name = "T_SYS_GROUP_ROLE", joinColumns = {
			@JoinColumn(name = "group_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "role_id", referencedColumnName = "id") })
	private Set<SysRole> roles = new HashSet<SysRole>(2);

	@Transient
	private List<Integer> roleIds = new ArrayList<Integer>(1);
	@Transient
	private List<SysRole> allRoles = new ArrayList<SysRole>(5);

	public SysGroup() {
		super();
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public Set<SysRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<SysRole> roles) {
		this.roles = roles;
	}

	public Set<SysUser> getUsers() {
		return users;
	}

	public void setUsers(Set<SysUser> users) {
		this.users = users;
	}

	public List<Integer> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<Integer> roleIds) {
		this.roleIds = roleIds;
	}

	public List<SysRole> getAllRoles() {
		return allRoles;
	}

	public void setAllRoles(List<SysRole> allRoles) {
		this.allRoles = allRoles;
	}

}
