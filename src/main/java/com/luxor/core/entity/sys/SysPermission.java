package com.luxor.core.entity.sys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.luxor.core.constant.Constants;
import com.luxor.core.entity.BaseEntity;

/**
 * 允许/权限
 * 
 * @author XinmingYan @data 2016-5-5 下午7:00:44 @versions 1.0
 * @packagePath com.luxro.cms.entiy.Permissions.java
 */
@Entity
@Table(name = "T_SYS_PERMISSION")
public class SysPermission extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;
	@NotEmpty(message = "权限标识不能为空")
	@Column(name = "permisson", unique = true, nullable = false)
	private String permisson;// 权限代码
	@NotNull(message = "权限名称不能为空")
	@Size(min = 2, max = 35, message = "权限名称长度在2到35岁之间")
	@Column(name = "name", unique = true)
	private String name;// 权限名称
	@Column(name = "type")
	private String type = Constants.BUTTON;// 权限类型
	@NotNull(message = "路径不能为空")
	@Column(name = "uri", unique = true, nullable = false)
	private String uri;// 路径
	@Column(name = "pid")
	private int pid;// 父节点ID
	@Column(name = "sort")
	private int sort;// 排序
	@Column(name = "status")
	private int status = 1;// (0-关闭; 1-激活)
	@Column(name = "force")
	private int force = 1;// (1-可删除; 0-不可删除)

	public SysPermission() {
		super();
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public String getPermisson() {
		return permisson;
	}

	public void setPermisson(String permisson) {
		this.permisson = permisson;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getForce() {
		return force;
	}

	public void setForce(int force) {
		this.force = force;
	}

}
