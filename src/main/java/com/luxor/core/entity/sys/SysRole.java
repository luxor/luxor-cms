package com.luxor.core.entity.sys;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.luxor.core.entity.BaseEntity;
import com.luxor.core.entity.TreeNodeVo;

/**
 * 角色组
 * 
 * @auditor XinmingYan @data 2016-6-29 下午5:45:09 @versions 1.0
 * @packagePath com.luxro.cms.entity.SysRole.java
 */
@Entity
@Table(name = "T_SYS_ROLE")
public class SysRole extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;
	@NotNull(message = "权限角色标识不能为空")
	@Column(name = "role", nullable = false, unique = true)
	private String role;// 角色代码
	@Size(min = 2, max = 15, message = "权限组名字长度在2到15岁之间")
	@Column(name = "name", unique = true)
	private String name;
	@Column(name = "details")
	private String details;
	@Column(name = "status")
	private int status = 1;// (0-注销;1-激活;)
	@Column(name = "sort")
	private int sort = 0;

	@ManyToMany(cascade = { CascadeType.DETACH })
	@JoinTable(name = "T_SYS_ROLE_PERMISSION", joinColumns = {
			@JoinColumn(name = "role_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "permission_id", referencedColumnName = "id") })
	private Set<SysPermission> permissions = new HashSet<SysPermission>(2);

	@Transient
	private List<Integer> permissionIds = new ArrayList<Integer>(1);
	@Transient
	private List<TreeNodeVo> allPermissions = new ArrayList<TreeNodeVo>(5);

	public SysRole() {
		super();
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public Set<SysPermission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<SysPermission> permissions) {
		this.permissions = permissions;
	}

	public List<Integer> getPermissionIds() {
		return permissionIds;
	}

	public void setPermissionIds(List<Integer> permissionIds) {
		this.permissionIds = permissionIds;
	}

	public List<TreeNodeVo> getAllPermissions() {
		return allPermissions;
	}

	public void setAllPermissions(List<TreeNodeVo> allPermissions) {
		this.allPermissions = allPermissions;
	}

}
