package com.luxor.core.entity.sys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.luxor.core.entity.BaseEntity;

@Entity
@Table(name = "T_SYS_GROUP_ROLE")
public class SysGroupRole extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;

	@Column(name = "role_id")
	private int roleId;
	@Column(name = "group_id")
	private int groupId;

	public SysGroupRole() {
		super();
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

}
