package com.luxor.core.entity.sys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.luxor.core.entity.BaseEntity;

@Entity
@Table(name = "T_SYS_USER_GROUP")
public class SysUserGroup extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;

	@Column(name = "user_id")
	private int userId;
	@Column(name = "group_id")
	private int groupId;

	public SysUserGroup() {
		super();
	}

	public SysUserGroup(int userId, int groupId) {
		super();
		this.userId = userId;
		this.groupId = groupId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

}
