package com.luxor.core.entity.sys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.luxor.core.entity.BaseEntity;

/**
 * 角色权限映射关系表
 * 
 * @auditor XinmingYan @data 2016-8-9 上午9:21:28 @versions 1.0
 * @packagePath com.luxor.core.entity.sys.SysRolePerissions.java
 */
@Entity
@Table(name = "T_SYS_ROLE_PERMISSION")
public class SysRolePermission extends BaseEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Column(name = "role_id")
	private int roleId;
	@Column(name = "permission_id")
	private int permissionId;

	public SysRolePermission() {
		super();
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}

}
