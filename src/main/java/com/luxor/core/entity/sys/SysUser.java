package com.luxor.core.entity.sys;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.luxor.core.constant.Status;
import com.luxor.core.entity.BaseEntity;

/**
 * 用户
 * 
 * @auditor XinmingYan @data 2016-6-30 上午10:40:17 @versions 1.0
 * @packagePath com.luxro.cms.entity.SysUser.java
 */
@Entity
@Table(name = "T_SYS_USER")
public class SysUser extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;
	@NotNull(message = "用户名不能为空")
	@Column(name = "username", nullable = false, unique = true)
	private String username;
	@Column(name = "password")
	private String password;
	@Column(name = "nickname")
	@NotNull(message = "姓名/昵称不能为空")
	private String nickname;
	@Column(name = "salt")
	private String salt;
	@Column(name = "email", unique = true)
	private String email;
	@Pattern(regexp = "[0-9]{11}", message = "电话号码必须是数字且长度为11.")
	@Column(name = "phone", unique = true)
	private String phone;
	@Column(name = "create_time")
	private Date createTime;
	@Column(name = "last_time")
	private Date lastTime;
	@Column(name = "expire_time")
	private Date expireTime; // 密码到期时间
	@Column(name = "invalid")
	private int invalid = 0;// (0-正常; 1-密码过期)
	@Column(name = "status")
	private int status = 0;// (0-激活; 1-注销; 2-待审核)

	@ManyToMany(cascade = { CascadeType.DETACH })
	@JoinTable(name = "T_SYS_USER_GROUP", joinColumns = {
			@JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "group_id", referencedColumnName = "id") })
	private List<SysGroup> groups = new ArrayList<SysGroup>(1);

	@Transient
	private List<Integer> groupIds = new ArrayList<Integer>(1);
	@Transient
	private List<SysGroup> allGroups = new ArrayList<SysGroup>(5);
	
	

	public SysUser() {
		super();
		this.createTime = new Date(System.currentTimeMillis());
	}

	public SysUser(String username, String password) {
		super();
		this.username = username;
		this.password = password;
		this.status = Status.NORMAL.key;
		this.createTime = new Date(System.currentTimeMillis());
	}

	public SysUser(String username, String password, String email, String phone) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.phone = phone;
		this.status = Status.NORMAL.key;
		this.createTime = new Date(System.currentTimeMillis());
	}

	/**
	 * 检测用户的运行状况
	 * 
	 * @return
	 */
	public boolean isCheck() {
		if (status != 0 || invalid != 0) {
			return false;
		}
		return true;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getLastTime() {
		return lastTime;
	}

	public void setLastTime(Date lastTime) {
		this.lastTime = lastTime;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public List<SysGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<SysGroup> groups) {
		this.groups = groups;
	}

	public int getInvalid() {
		return invalid;
	}

	public void setInvalid(int invalid) {
		this.invalid = invalid;
	}

	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}

	public List<Integer> getGroupIds() {
		return groupIds;
	}

	public void setGroupIds(List<Integer> groupIds) {
		this.groupIds = groupIds;
	}

	public List<SysGroup> getAllGroups() {
		return allGroups;
	}

	public void setAllGroups(List<SysGroup> allGroups) {
		this.allGroups = allGroups;
	}

}
