package com.luxor.core.entity.sys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.luxor.core.entity.BaseEntity;

/**
 * 系统配置
 * 
 * @auditor XinmingYan @data 2016-8-9 上午9:22:02 @versions 1.0
 * @packagePath com.luxor.core.entity.sys.SysConfig.java
 */
@Entity
@Table(name = "T_SYS_CONFIG")
public class SysConfig extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;

	// 网站名称
	public static final String LUXOR_SEO_TITLE = "luxor_seo_title";
	// 网站关键字
	public static final String LUXOR_SEO_KEYWORDS = "luxor_seo_keywords";
	// 网站描述
	public static final String LUXOR_SEO_DESCRIPTION = "luxor_seo_description";
	// 公司地址
	public static final String LUXOR_COMPANY_ADDRESS = "luxor_company_address";
	// 公司名称
	public static final String LUXOR_COMPANY_NAME = "luxor_company_name";
	// 网站管理员邮箱
	public static final String LUXOR_ADMIN_EMAIL = "luxor_admin_email";
	// 网站备案编号
	public static final String LUXOR_MIIBEIAN = "luxor_miibeian";
	// 网站主题
	public static final String LUXOR_THEME = "luxor_theme";

	@NotNull(message = "参数标识不能为空")
	@Column(name = "key", nullable = false, unique = true)
	private String key;

	@NotNull(message = "参数值不能为空")
	@Column(name = "value", nullable = false)
	private String value;

	@Column(name = "details")
	private String details;

	@Column(name = "canDel")
	private int canDel = 1;// (1-可删除; 0-不可删除)

	public SysConfig() {
		super();
		this.id = 0;
	}

	public SysConfig(String key, String value, String details) {
		super();
		this.key = key;
		this.value = value;
		this.details = details;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public int getCanDel() {
		return canDel;
	}

	public void setCanDel(int canDel) {
		this.canDel = canDel;
	}

}
