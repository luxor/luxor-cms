package com.luxor.core.entity;

/**
 * 树形菜单节点
 * 
 * @auditor XinmingYan @data 2018-8-24 上午11:15:09 @versions 1.0
 * @packagePath com.luxor.core.entity.TreeNodeVo.java
 */
public class TreeNodeVo {
	/**
	 * 节点id
	 */
	private int id;
	/**
	 * 父节点id
	 */
	private int pId;
	/**
	 * 节点名称
	 */
	private String name;
	/**
	 * 顺序号
	 */
	private int sort;

	/**
	 * 请求
	 */
	private String href;

	/**
	 * 禁止选中?
	 */
	private boolean chkDisabled;
	/**
	 * 选中吗？
	 */
	private boolean checked;
	/**
	 * 展开吗?
	 */
	private boolean open;

	public TreeNodeVo() {
		super();
	}

	public TreeNodeVo(int id, int pId, String name) {
		super();
		this.id = id;
		this.pId = pId;
		this.name = name;
	}

	public TreeNodeVo(int id, int pId, String name, boolean checked, boolean open) {
		super();
		this.id = id;
		this.pId = pId;
		this.name = name;
		this.checked = checked;
		this.open = open;
	}

	public TreeNodeVo(int id, int pId, String name, boolean chkDisabled, boolean checked, boolean open) {
		super();
		this.id = id;
		this.pId = pId;
		this.name = name;
		this.chkDisabled = chkDisabled;
		this.checked = checked;
		this.open = open;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getpId() {
		return pId;
	}

	public void setpId(int pId) {
		this.pId = pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public boolean isChkDisabled() {
		return chkDisabled;
	}

	public void setChkDisabled(boolean chkDisabled) {
		this.chkDisabled = chkDisabled;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	@Override
	public String toString() {
		return "TreeNodeVo [id=" + id + ", pId=" + pId + ", name=" + name + ", sort=" + sort + ", href=" + href
				+ ", chkDisabled=" + chkDisabled + ", checked=" + checked + ", open=" + open + "]";
	}

}
