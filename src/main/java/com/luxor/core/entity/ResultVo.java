package com.luxor.core.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * 统一的响应结果
 * 
 * @auditor XinmingYan @data 2016-8-22 上午11:15:09 @versions 1.0
 * @packagePath com.luxor.core.entity.JsonValue.java
 */
public class ResultVo
{
	// 响应是否请求成功
	private boolean success = true;
	// 响应的状态码
	private int code = 200;
	// 响应的消息体
	private Object date = "未填充响应内容";
	// 错误原因
	private Set<Object> errors = new HashSet<Object>();

	public ResultVo()
	{
		super();
	}

	public ResultVo(boolean success)
	{
		super();
		this.success = success;
		this.code = success == true ? 200 : 500;
	}

	public ResultVo(boolean success, Object content)
	{
		this.success = success;
		if (success)
		{
			this.code = 200;
			this.date = content;
		} else
		{
			this.code = 500;
			errors.add(content);
		}
	}

	public boolean hasError()
	{
		return this.errors.size() > 0 ? true : false;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public int getCode()
	{
		return code;
	}

	public void setCode(int code)
	{
		this.code = code;
	}

	public Object getData()
	{
		return date;
	}

	public void setData(Object date)
	{
		this.date = date;
	}

	public Set<Object> getErrors()
	{
		return errors;
	}

	public void setErrors(Set<Object> errors)
	{
		this.errors = errors;
	}

	public void setError(String error)
	{
		getErrors().add(error);
	}
}
