package com.luxor.core.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.luxor.core.constant.Constants;
import com.luxor.core.service.sys.SysConfigService;
import com.luxor.core.utils.HttpUtils;

/**
 * 全局性SpringMVC过滤器
 * 
 * @auditor XinmingYan @data 2016-7-31 下午11:27:19 @versions 1.0
 * @packagePath com.luxro.core.interceptor.GlobalInterceptor.java
 */
@Component
public class GlobalInterceptor implements HandlerInterceptor {
	@Autowired
	SysConfigService sysConfigService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object, ModelAndView mv)
			throws Exception {
		// 排除非视图请求
		if (mv == null) {
			return;
		}

		// 根目录URL
		String basePath = HttpUtils.getBasePath(request);
		mv.addObject(Constants.BASE_PATH, basePath);
		// 前端插件
		mv.addObject(Constants.BASE_PLUGINS_PATH, basePath + "/static/plugins");
		// 后台模版
		mv.addObject(Constants.BASE_MANAGE_PATH, basePath + "/static/skin/manage");
		// 前台模板
		String skin = sysConfigService.getTheme();
		mv.addObject(Constants.BASE_TEMPLATE_PATH, basePath + "/static/skin/template/" + skin);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object,
			Exception exception) throws Exception {
	}

}
