package com.luxor.core.constant;

/**
 * Boolean枚举类
 * 
 * @author Administrator
 *
 */
public enum Active {
	YES(0, "否"), NO(1, "是");

	public final int key;
	public final String value;

	public static String getValue(int key) {
		for (Active element : Active.values()) {
			if (element.key == key) {
				return element.value;
			}
		}
		return null;
	}

	public static int getKey(String value) {
		for (Active element : Active.values()) {
			if (element.value == value) {
				return element.key;
			}
		}
		return 0;
	}

	/**
	 * 枚举构造器
	 * 
	 * @param index
	 * @param name
	 */
	private Active(int index, String name) {
		this.key = index;
		this.value = name;
	}
}
