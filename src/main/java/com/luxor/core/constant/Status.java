package com.luxor.core.constant;

/**
 * 激活状态的枚举类
 * 
 * @author Administrator
 *
 */
public enum Status {
	FAILURE(0, "失效"), NORMAL(1, "正常"),VAGUE(2, "待审核");

	public final int key;
	public final String value;

	public static boolean isEnabled(int key) {
		if (NORMAL.key == key) {
			return true;
		}
		return false;
	}

	public static String getValue(int key) {
		for (Status element : Status.values()) {
			if (element.key == key) {
				return element.value;
			}
		}
		return null;
	}

	public static int getKey(String value) {
		for (Status element : Status.values()) {
			if (element.value == value) {
				return element.key;
			}
		}
		return 0;
	}

	/**
	 * 枚举构造器
	 * 
	 * @param index
	 * @param name
	 */
	private Status(int key, String value) {
		this.key = key;
		this.value = value;
	}

}
