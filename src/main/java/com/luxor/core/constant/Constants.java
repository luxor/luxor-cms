package com.luxor.core.constant;

/**
 * 系统常量
 * 
 * @auditor XinmingYan @data 2016-7-22 下午2:53:36 @versions 1.0
 * @packagePath com.luxro.core.constant.Constants.java
 */
public class Constants
{
	/**
	 * 请求动作/消息
	 */
	public static final String OP = "op";
	public static final String MSG = "msg";
	public static final String ERROR_MODEL = "error_model";
	public static final String ERROR_COLE = "error_cole";

	/**
	 * 资源类型
	 */
	public static final String BUTTON = "button";
	public static final String MENU = "menu";


	/**
	 * 全局路径
	 */
	public static final String BASE_PATH = "BASE_PATH";
	
	/**
	 * 公共插件路径
	 */
	public static final String BASE_PLUGINS_PATH = "BASE_PLUGINS_PATH";
	
	/**
	 * 后台模版
	 */
	public static final String BASE_MANAGE_PATH = "BASE_MANAGE_PATH";
	
	/**
	 * 前台模版
	 */
	public static final String BASE_TEMPLATE_PATH = "BASE_TEMPLATE_PATH";
}
