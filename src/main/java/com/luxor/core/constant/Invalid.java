package com.luxor.core.constant;

public enum Invalid {
	VALID(0, "有效"), STALE(1, "已过期");

	public final int key;
	public final String value;

	public static boolean isInvalid(int key) {
		if (VALID.key == key) {
			return true;
		}
		return false;
	}
	
	private Invalid(int key, String value) {
		this.key = key;
		this.value = value;
	}

}
