package com.luxor.core.web.manage;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.luxor.core.entity.sys.SysGroup;
import com.luxor.core.entity.sys.SysRole;
import com.luxor.core.service.BaseService;
import com.luxor.core.service.sys.SysGroupService;
import com.luxor.core.service.sys.SysRoleService;
import com.luxor.core.web.AbstractCurdAction;

/**
 * 系统组管理
 * 
 * @auditor XinmingYan @data 2018-5-11 下午9:05:13 @versions 1.0
 * @packagePath com.luxor.core.web.manage.ManageConfigAction.java
 */
@Controller
@RequestMapping("/manage/sys_group")
public class ManageGroupAction extends AbstractCurdAction<SysGroup, Integer>
{
	@Autowired
	private SysGroupService sysGroupService;

	@Autowired
	private SysRoleService sysRoleService;
	
	@Override
	protected BaseService<SysGroup, Integer> getService() {
		return sysGroupService;
	}

	@Override
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public ModelAndView save(@Validated SysGroup entity, BindingResult result) {
		if (result.hasErrors()) {
			return failure(getEditView(), entity, result.getFieldErrors());
		} else {
			for (Integer id : entity.getRoleIds()) {
				SysRole sysRole = sysRoleService.getEntity(id);
				entity.getRoles().add(sysRole);
			}
			getService().save(entity);
			List<SysRole> allRoles = sysRoleService.findAll();
			entity.setAllRoles(allRoles);
			return succeed(getEditView(), entity);
		}
	}

	@Override
	@RequestMapping(value = "edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam(required = false) Integer id) {
		SysGroup entity = new SysGroup();
		if (id != 0) {
			entity = getService().getEntity(id);
		}
		List<SysRole> allRoles = sysRoleService.findAll();
		entity.setAllRoles(allRoles);

		ModelAndView mv = new ModelAndView();
		mv.setViewName(getEditView());
		mv.addObject(CONTENT, entity);
		return mv;
	}
	
	@Override
	protected String getContextView() {
		return themeService.getManageUri("group");
	}
}
