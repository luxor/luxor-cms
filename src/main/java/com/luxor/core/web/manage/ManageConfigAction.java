package com.luxor.core.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.luxor.core.entity.sys.SysConfig;
import com.luxor.core.service.BaseService;
import com.luxor.core.service.sys.SysConfigService;
import com.luxor.core.web.AbstractCurdAction;

/**
 * 系统参数管理
 * 
 * @auditor XinmingYan @data 2018-5-11 下午9:05:13 @versions 1.0
 * @packagePath com.luxor.core.web.manage.ManageConfigAction.java
 */
@Controller
@RequestMapping("/manage/sys_config")
public class ManageConfigAction extends AbstractCurdAction<SysConfig, Integer>
{
	@Autowired
	private SysConfigService sysConfigService;

	@Override
	protected BaseService<SysConfig, Integer> getService() {
		return sysConfigService;
	}

	@Override
	protected String getContextView() {
		return themeService.getManageUri("config");
	}
}
