package com.luxor.core.web.manage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.luxor.core.service.sys.SysSessionService;
import com.luxor.core.web.BaseAction;

@Controller
@RequestMapping("/manage/sys_session")
public class ManageSessionAction extends BaseAction
{
	private static Logger logger = LoggerFactory.getLogger(ManageSessionAction.class);

	@Autowired
	private SysSessionService sysSessionService;

	public void kickOut(String username)
	{
		logger.info("Enter the kickOut request. / username:{}", username);
		sysSessionService.kickOut(username);
	}
}
