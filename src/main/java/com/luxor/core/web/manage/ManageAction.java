package com.luxor.core.web.manage;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.luxor.core.annotation.Principal;
import com.luxor.core.plugin.shiro.User;
import com.luxor.core.service.sys.SysUserService;
import com.luxor.core.web.BaseAction;

/**
 * 进入后台管理
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/manage")
public class ManageAction extends BaseAction {
	@Resource
	SysUserService sysUserService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String manage() {
		logger.info("Enter the manage home page");
		return themeService.getManageIndex();
	}

	/**
	 * 默认后台首页
	 * 
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/default", method = RequestMethod.GET)
	public String home() {
		logger.info("Enter the manage default page");
		return themeService.getManageUri("default");
	}

	/**
	 * 关于我们
	 * 
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String about() {
		logger.info("Enter the about page");
		return themeService.getManageUri("about");
	}

	/**
	 * 个人资料
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public String user(@Principal User user, ModelMap modelMap) {
		logger.info("Enter the user page");
		return themeService.getManageUri("user");
	}
}
