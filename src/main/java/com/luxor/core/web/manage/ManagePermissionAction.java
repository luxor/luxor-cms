package com.luxor.core.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.luxor.core.entity.sys.SysPermission;
import com.luxor.core.service.BaseService;
import com.luxor.core.service.sys.SysPermissionService;
import com.luxor.core.web.AbstractCurdAction;

/**
 * 管理员管理
 * 
 * @auditor XinmingYan @data 2016-9-19 上午9:05:13 @versions 1.0
 * @packagePath com.luxor.core.web.manage.ManageUsersAction.java
 */
@Controller
@RequestMapping("/manage/sys_permission")
public class ManagePermissionAction extends AbstractCurdAction<SysPermission, Integer>
{
	@Autowired
	private SysPermissionService sysPerissionService;

	@Override
	protected BaseService<SysPermission, Integer> getService() {
		return sysPerissionService;
	}

	@Override
	protected String getContextView() {
		return themeService.getManageUri("permission");
	}
}
