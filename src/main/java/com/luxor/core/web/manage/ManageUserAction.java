package com.luxor.core.web.manage;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.luxor.core.entity.sys.SysGroup;
import com.luxor.core.entity.sys.SysUser;
import com.luxor.core.service.BaseService;
import com.luxor.core.service.sys.SysGroupService;
import com.luxor.core.service.sys.SysUserService;
import com.luxor.core.web.AbstractCurdAction;

/**
 * 管理员管理
 * 
 * @auditor XinmingYan @data 2016-9-19 上午9:05:13 @versions 1.0
 * @packagePath com.luxor.core.web.manage.ManageUsersAction.java
 */
@Controller
@RequestMapping("/manage/sys_user")
public class ManageUserAction extends AbstractCurdAction<SysUser, Integer> {
	@Autowired
	private SysUserService sysUserService;

	@Resource
	private SysGroupService sysGroupService;

	@Override
	protected BaseService<SysUser, Integer> getService() {
		return sysUserService;
	}

	@Override
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public ModelAndView save(@Validated SysUser entity, BindingResult result) {
		if (result.hasErrors()) {
			return failure(getEditView(), entity, result.getFieldErrors());
		} else {
			for (Integer id : entity.getGroupIds()) {
				SysGroup sysGroup = sysGroupService.getEntity(id);
				entity.getGroups().add(sysGroup);
			}
			getService().save(entity);
			List<SysGroup> allGroups = sysGroupService.findAll();
			entity.setAllGroups(allGroups);
			return succeed(getEditView(), entity);
		}
	}

	@Override
	@RequestMapping(value = "edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam(required = false) Integer id) {
		SysUser entity = new SysUser();
		if (id != 0) {
			entity = getService().getEntity(id);
		}
		List<SysGroup> allGroups = sysGroupService.findAll();
		entity.setAllGroups(allGroups);

		ModelAndView mv = new ModelAndView();
		mv.setViewName(getEditView());
		mv.addObject(CONTENT, entity);
		return mv;
	}

	@Override
	protected String getContextView() {
		return themeService.getManageUri("user");
	}
}
