package com.luxor.core.web.manage;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.luxor.core.entity.TreeNodeVo;
import com.luxor.core.entity.sys.SysPermission;
import com.luxor.core.entity.sys.SysRole;
import com.luxor.core.service.BaseService;
import com.luxor.core.service.sys.SysPermissionService;
import com.luxor.core.service.sys.SysRoleService;
import com.luxor.core.web.AbstractCurdAction;

/**
 * 系统角色管理
 * 
 * @auditor XinmingYan @data 2018-5-11 下午9:05:13 @versions 1.0
 * @packagePath com.luxor.core.web.manage.ManageConfigAction.java
 */
@Controller
@RequestMapping("/manage/sys_role")
public class ManageRoleAction extends AbstractCurdAction<SysRole, Integer> {
	@Autowired
	private SysRoleService sysRoleService;

	@Autowired
	private SysPermissionService sysPermissionService;

	@Override
	protected BaseService<SysRole, Integer> getService() {
		return sysRoleService;
	}

	@Override
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public ModelAndView save(@Validated SysRole entity, BindingResult result) {
		if (result.hasErrors()) {
			return failure(getEditView(), entity, result.getFieldErrors());
		} else {
			processTreeNodes(entity);
			getService().save(entity);
			return succeed(getEditView(), entity);
		}
	}

	@Override
	@RequestMapping(value = "edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam(required = false) Integer id) {
		SysRole entity = new SysRole();
		if (id != 0) {
			entity = getService().getEntity(id);
		}
		processTreeNodes(entity);
		ModelAndView mv = new ModelAndView();
		mv.setViewName(getEditView());
		mv.addObject(CONTENT, entity);
		return mv;
	}

	/**
	 * 构建树形列表数据
	 */
	private void processTreeNodes(SysRole entity) {
		for (Integer id : entity.getPermissionIds()) {
			SysPermission sysPermission = sysPermissionService.getEntity(id);
			entity.getPermissions().add(sysPermission);
		}
		List<Integer> pIds = entity.getPermissions().stream().map(SysPermission::getId).collect(Collectors.toList());
		List<SysPermission> allps = sysPermissionService.findAll();
		entity.setAllPermissions(allps.stream().sorted(Comparator.comparing(SysPermission::getSort)).map(p -> {
			boolean chkDisabled = p.getForce() == 1;
			boolean checked = pIds.contains(p.getId()) || p.getForce() == 1;
			boolean open = p.getId() > 1 ? false : true;
			return new TreeNodeVo(p.getId(), p.getPid(), p.getName(), chkDisabled, checked, open);
		}).collect(Collectors.toList()));
	}

	@Override
	protected String getContextView() {
		return themeService.getManageUri("role");
	}
}
