package com.luxor.core.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.FieldError;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.luxor.core.entity.ResultVo;
import com.luxor.core.service.ThemeService;

/**
 * 控制层的基类
 * 
 * @auditor XinmingYan @data 2016-9-7 下午3:08:38 @versions 1.0
 * @packagePath com.luxor.core.web.BaseAction.java
 */
public class BaseAction extends MultiActionController {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	// 错误提示消息前缀
	protected static final String ERR = "ERR_";
	// 获取响应结果的标识名
	protected final String CONTENT = "result";

	@Autowired
	protected HttpServletResponse response;
	@Autowired
	protected HttpServletRequest request;
	@Autowired
	protected ThemeService themeService;

	/**
	 * 成功的JSON对象
	 * 
	 * @param message
	 * @return
	 */
	protected Object succeed(Object message) {
		return new ResultVo(true, message);
	}

	/**
	 * 成功后的视图转向
	 * 
	 * @param message
	 * @return
	 */
	protected ModelAndView succeed(String viewName, Object data) {
		ModelMap model = new ModelMap();
		model.addAttribute(CONTENT, data);
		return new ModelAndView(viewName, model);
	}

	/**
	 * 失败的JSON对象
	 * 
	 * @param message
	 * @return
	 */
	protected Object failure(String message) {
		return new ResultVo(false, message);
	}

	/**
	 * 校验失败的JSON对象
	 * 
	 * @param message
	 * @return
	 */
	protected Object failure(List<FieldError> errors) {
		ResultVo result = new ResultVo(false);
		for (FieldError fieldError : errors) {
			result.setError(fieldError.getDefaultMessage());
		}
		return result;
	}

	/**
	 * 校验失败的视图转向
	 * 
	 * @param message
	 * @return
	 */
	protected ModelAndView failure(String viewName, Object data, List<FieldError> errors) {
		ModelMap model = new ModelMap();
		for (FieldError fieldError : errors) {
			logger.warn("ERR_{}: {}", fieldError.getField(), fieldError.getDefaultMessage());
			model.addAttribute(ERR + fieldError.getField(), fieldError.getDefaultMessage());
		}
		model.addAttribute(CONTENT, data);
		return new ModelAndView(viewName, model);
	}

}
