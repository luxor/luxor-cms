package com.luxor.core.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.luxor.core.service.sys.SysConfigService;

/**
 * 进入CMS主页界面
 * @author Administrator
 *
 */
@Controller
public class IndexAction extends BaseAction
{
	@Resource
	SysConfigService sysConfigService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String cms(ModelMap modelMap)
	{
		logger.info("Enter the home page");
		return home(modelMap);
	}

	@RequestMapping(value = "/index.html", method = RequestMethod.GET)
	public String home(ModelMap modelMap)
	{
		logger.info("Enter the home page");
		return themeService.getIndex();
	}

	@RequestMapping(value = "/405.html", method = RequestMethod.GET)
	public String requestNotAllowed()
	{
		return themeService.get405();
	}

	@RequestMapping(value = "/error.html", method = RequestMethod.GET)
	public String error()
	{
		return themeService.getError();
	}
}
