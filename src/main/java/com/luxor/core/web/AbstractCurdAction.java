package com.luxor.core.web;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.luxor.core.entity.DataTableVo;
import com.luxor.core.exception.sys.ParamNullException;
import com.luxor.core.exception.sys.ServiceException;
import com.luxor.core.plugin.search.QueryParam;
import com.luxor.core.plugin.search.SearchHolder;
import com.luxor.core.service.BaseService;

/**
 * 实现增删改查的通用控制器
 * 
 * @auditor XinmingYan @data 2016-9-7 下午3:07:01 @versions 1.0
 * @packagePath com.luxor.core.web.CurdAction.java
 */
public abstract class AbstractCurdAction<T, PK extends Serializable> extends BaseAction {
	// 逻辑视图名
	private String editView = "edit";// 增加或修改
	private String showView = "view";// 查看
	private String listView = "list";// 查看列表

	// 默认每页显示大小
	private final int defaultPageSize = 20;

	/**
	 * 当前业务模块的服务层
	 * 
	 * @return
	 */
	protected abstract BaseService<T, PK> getService();

	/**
	 * 视图的模块上下文(当前模块视图所在包的名称)
	 * 
	 * @return
	 */
	protected abstract String getContextView();

	/**
	 * to: 数据列表
	 * 
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView toList() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName(getListView());
		return mv;
	}

	/**
	 * 管理列表/查看所有
	 * 
	 * @param page 当前页数
	 * @param pageSize 每页总数
	 * @param sort 排序的列
	 * @param order 顺序
	 * @return
	 */
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ModelAndView list(
			@RequestParam(value = "pageNumber", defaultValue = "1") int pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "20") int pageSize,
			@RequestParam(value = "sortName", defaultValue = "") String sortName,
			@RequestParam(value = "sortOrder", defaultValue = "ASC") String sortOrder) {
		List<QueryParam> filters = SearchHolder.getQueryParams(request);
		Page<T> datas = getService().findAll(getPageable(pageNumber, pageSize, getSort(sortName, sortOrder)), filters);
		ModelAndView mv = new ModelAndView();
		mv.addObject(CONTENT, datas);
		mv.setViewName(getListView());
		return mv;
	}

	/**
	 * 列表数据
	 * 
	 * @param page 当前页数
	 * @param pageSize 每页总数
	 * @param sort 排序的列
	 * @param order 顺序
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "listAjax", method = RequestMethod.GET)
	public Object listAjax(
			@RequestParam(value = "pageNumber", defaultValue = "1") int pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "20") int pageSize,
			@RequestParam(value = "sortName", defaultValue = "") String sortName,
			@RequestParam(value = "sortOrder", defaultValue = "ASC") String sortOrder) {
		List<QueryParam> filters = SearchHolder.getQueryParams(request);
		DataTableVo<T> data = getService().findDataTable(
				getPageable(pageNumber, pageSize, getSort(sortName, sortOrder)), filters);
		return succeed(data);
	}

	/**
	 * to: 添加/修改
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam(required = false) PK id) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName(getEditView());
		T entity = null;
		if (id == null || StringUtils.isBlank(id.toString()) || (Integer) id == 0) {
			entity = getEntityInstance();
		} else {
			entity = getService().getEntity(id);
		}
		mv.addObject(CONTENT, entity);
		return mv;
	}

	/**
	 * 保存/添加/修改
	 * 
	 * @param entity
	 * @return
	 */
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public ModelAndView save(@Validated T entity, BindingResult result) {
		if (result.hasErrors()) {
			return failure(getEditView(), entity, result.getFieldErrors());
		} else {
			getService().save(entity);
			return succeed(getEditView(), entity);
		}
	}

	/**
	 * to: 查看
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam PK id) {
		ModelAndView mv = new ModelAndView();
		mv.addObject(CONTENT, getService().getEntity(id));
		mv.setViewName(getShowView());
		return mv;
	}

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "delete", method = RequestMethod.GET)
	public Object delete(HttpServletRequest request, HttpServletResponse response,
			@RequestParam String id) {
		getService().delete(getIdArray(id));
		return succeed("删除成功!");
	}

	// /////////////////////////////////////////

	public Sort getSort(String sort, String order) {
		if (StringUtils.isBlank(sort)) {
			return null;
		}
		if (Direction.ASC.toString().equalsIgnoreCase(order) || StringUtils.isBlank(order)) {
			return new Sort(Direction.ASC, sort.split(","));
		} else {
			return new Sort(Direction.DESC, sort.split(","));
		}
	}

	public Pageable getPageable(int page) {
		return getPageable(page, 20, null);
	}

	public Pageable getPageable(int page, int pageSize) {
		return getPageable(page, pageSize, null);
	}

	public Pageable getPageable(int page, int pageSize, Sort sort) {
		page = (page == 0) ? 1 : page;
		pageSize = (pageSize == 0) ? defaultPageSize : pageSize;
		return new PageRequest(page - 1, pageSize, sort);
	}

	@SuppressWarnings("unchecked")
	public T getEntityInstance() {
		T instance = null;
		try {
			instance = (T) getGenericClass(0).newInstance();
		} catch (Exception e) {
			throw new ServiceException("newEntityInstance_failure", e);
		}
		return instance;
	}

	@SuppressWarnings({ "unchecked" })
	public PK[] getIdArray(String id) {
		if (StringUtils.isNotBlank(id)) {
			String[] ids = id.split(",");
			if ((Class<PK>) getGenericClass(1) == String.class) {
				return (PK[]) ids;
			} else {
				List<Integer> list = new LinkedList<Integer>();
				for (String tmpId : ids) {
					list.add(Integer.parseInt(tmpId));
				}
				return (PK[]) list.toArray(new Integer[list.size()]);
			}
		}
		throw new ParamNullException("主键值不能为空!");
	}

	private Class<?> getGenericClass(int i) {
		Type genType = getClass().getGenericSuperclass();
		Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
		return (Class<?>) params[i];
	}

	// ////////////////////////////////////////////////////////////////////////////////

	private String getContext() {
		String context = getContextView();
		return StringUtils.isBlank(context) ? "" : getContextView() + "/";
	}

	public String getShowView() {
		return getContext() + showView;
	}

	public String getEditView() {
		return getContext() + editView;
	}

	public String getListView() {
		return getContext() + listView;
	}

	public void setShowView(String showView) {
		this.showView = showView;
	}

	public void setEditView(String editView) {
		this.editView = editView;
	}

	public void setListView(String listView) {
		this.listView = listView;
	}

}
