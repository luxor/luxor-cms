package com.luxor.core.web;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.luxor.core.utils.FileUtils;
import com.luxor.core.utils.UuidUtiles;

/**
 * 基于MultipartFile实现的文件上传功能
 * 
 * @auditor XinmingYan @data 2016-8-29 下午1:02:38 @versions 1.0
 * @packagePath com.luxor.core.web.UploadFile.java
 */
@Controller
@RequestMapping("/store")
public class StoreAction extends BaseAction {
	private static final String SEPARATOR = "/";
	private static final String DOT = ".";
	private static final String LINE = "-";
	private static final SimpleDateFormat PREFIX_FPRMAT = new SimpleDateFormat("yyyy/MM/dd/");

	/**
	 * 批量上传
	 * 
	 * @param file
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public Object batchUpload(@RequestParam MultipartFile[] file) {
		logger.info("Enter the batchUpload Request.");
		if (file == null || file.length == 0) {
			logger.warn("UploadFile is null!");
			return failure("失败,请选择文件后再上传");
		}
		try {
			for (MultipartFile multipartFile : file) {
				// 上传文件的原名
				String fileName = multipartFile.getOriginalFilename();
				// 文件存放的唯一路径
				File saveFile = generateUploadFile(fileName);
				FileUtils.checkFile(saveFile);
				// 复制上传的文件到服务器
				multipartFile.transferTo(saveFile);
				logger.info("upload file:{}", fileName);
			}
			return succeed("成功,已经上传完所有文件.");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return failure("文件上传失败,请联系管理员");
		}
	}

	/**
	 * 批量下载
	 * 
	 * @param filePath
	 *            "2017/12/20/Hls8as3.ptf"
	 */
	@ResponseBody
	@RequestMapping(value = "/download", method = RequestMethod.POST)
	public Object exportExcelTemplate(@RequestParam String filePath) throws IOException {
		String fileName = URLEncoder.encode(filePath, "UTF-8");
		response.setContentType("application/msexcel;charset=UTF-8");
		response.setHeader("Content-Disposition", "attachment;filename=".concat(fileName));
		response.setHeader("Connection", "close");
		response.setHeader("Content-Type", "application/octet-stream");
		byte[] buffer = new byte[1024];
		BufferedInputStream input = null;
		OutputStream output = null;
		try {
			input = new BufferedInputStream(new FileInputStream(generateDownloadFile(filePath)));
			output = response.getOutputStream();
			while (input.read(buffer) != -1) {
				output.write(buffer);
			}
			output.flush();
			return succeed("成功,已经下载完所有文件.");
		} catch (IOException e) {
			logger.error("文件下载失败!", e);
			return failure(String.format("失败,文件下载失败. 原因：%s", e.getMessage()));
		} finally {
			IOUtils.closeQuietly(input);
			IOUtils.closeQuietly(output);
		}
	}

	// //////////////////////////////////////////////////////////////////////////////////////
	// 私有方法

	/**
	 * 生成唯一的上传文件
	 * 
	 * @param fileName 将要转换的文件名。 例如："阿里巴巴Java编程规范.pdf"
	 * @return 系统唯一的文件路径。
	 *         例如："/luxor/upload/2017/04/30/阿里巴巴Java开发规范说明书-OZE9VNnC.pdf"
	 */
	private File generateUploadFile(String fileName) {
		if (fileName == null || fileName.lastIndexOf(DOT) == -1) {
			new IllegalArgumentException("不是合法的文件名称");
		}
		String fileTitle = fileName.split(DOT)[0];
		String fileSuffix = fileName.substring(fileName.lastIndexOf(DOT));
		String uploadFolder = themeService.getUpload();
		StringBuilder uniquePath = new StringBuilder(uploadFolder);
		if (!uploadFolder.endsWith(SEPARATOR)) {
			uniquePath.append(SEPARATOR);
		}
		uniquePath.append(PREFIX_FPRMAT.format(new Date()));
		uniquePath.append(fileTitle).append(LINE);
		uniquePath.append(UuidUtiles.randomUUID8());
		uniquePath.append(fileSuffix);
		return new File(uniquePath.toString());
	}

	/**
	 * 生成下载文件
	 * 
	 * @param filePath 要下载的文件.例如："2017/12/20/Hls8as3.ptf"
	 * @return
	 */
	private File generateDownloadFile(String filePath) {
		if (filePath == null || filePath.lastIndexOf(DOT) == -1) {
			throw new IllegalArgumentException("不是合法的文件名称");
		}
		String uploadFolder = themeService.getUpload();
		StringBuilder uniquePath = new StringBuilder(uploadFolder);
		uniquePath.append(filePath);
		return new File(uniquePath.toString());
	}

}
