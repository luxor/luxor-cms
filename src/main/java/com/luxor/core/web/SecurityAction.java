package com.luxor.core.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.ExpiredCredentialsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.luxor.core.constant.Constants;
import com.luxor.core.entity.sys.SysUser;
import com.luxor.core.exception.sys.JCaptchaValidateException;
import com.luxor.core.service.sys.SysUserService;

/**
 * 用户安全认证
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/security")
public class SecurityAction extends BaseAction {
	public static final String DEFAULT_ERROR_KEY_ATTRIBUTE_NAME = "shiroLoginFailure";

	@Resource
	SysUserService sysUserService;

	@RequestMapping(value = "/login")
	public ModelAndView toLogin(HttpServletRequest request, ModelMap model) {
		logger.info("Enter the login page");
		String error = (String) request.getAttribute(DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
		if (error != null) {
			if (JCaptchaValidateException.class.getName().equals(error)) {
				model.addAttribute(Constants.MSG, "验证码不正确");
			} else if (IncorrectCredentialsException.class.getName().equals(error)) {
				model.addAttribute(Constants.MSG, "密码不正确");
			} else if (UnknownAccountException.class.getName().equals(error)) {
				model.addAttribute(Constants.MSG, "帐号不存在");
			} else if (LockedAccountException.class.getName().equals(error)) {
				model.addAttribute(Constants.MSG, "帐号被锁定");
			} else if (DisabledAccountException.class.getName().equals(error)) {
				model.addAttribute(Constants.MSG, "帐号被禁用或待审核");
			} else if (ExcessiveAttemptsException.class.getName().equals(error)) {
				model.addAttribute(Constants.MSG, "登录失败次数过多");
			} else if (ExpiredCredentialsException.class.getName().equals(error)) {
				model.addAttribute(Constants.MSG, "密码已过期");
			} else {
				model.addAttribute(Constants.MSG, "未知异常,请联系管理员");
			}
		}
		return new ModelAndView(themeService.getLogin(), model);
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout() {
		logger.info("Enter the logout page");
		return themeService.getLogin();
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String toRegister() {
		logger.info("Enter the register page");
		return themeService.getManageUri("security/register");
	}

	@RequestMapping(value = "/register.do", method = RequestMethod.GET)
	public ModelAndView register(ModelMap model, @Valid SysUser sysUser, BindingResult result) {
		if (!result.hasErrors() && sysUserService.createUser(sysUser)) {
			return new ModelAndView(themeService.getLogin(), model);
		}
		return failure(themeService.getManageUri("security/register"),sysUser, result.getFieldErrors());
	}

	@RequestMapping(value = "/changePassword", method = RequestMethod.GET)
	public ModelAndView toChangePassword() {
		logger.info("Enter the changePassword page");
		return new ModelAndView(themeService.getManageUri("security/password"));
	}

	@RequestMapping(value = "/changePassword.do", method = RequestMethod.POST)
	public ModelAndView changePassword(@RequestParam String username,
			@RequestParam String oldPassword, @RequestParam String newPassword) {
		sysUserService.changePassword(username, oldPassword, newPassword);
		return new ModelAndView(themeService.getLogin());
	}
}
