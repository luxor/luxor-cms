package com.luxor.core.repository;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

/**
 * 资源仓库工厂Bean,用于覆盖Jpa默认的资源仓库工厂类
 * 
 * @auditor XinmingYan @data 2016-7-21 上午12:41:03 @versions 1.0
 * @packagePath com.luxro.cms.repository.SimpleBaseRepositoryFactoryBean.java
 */
public class SimpleRepositoryFactoryBean<R extends JpaRepository<M, ID>, M, ID extends Serializable>
		extends JpaRepositoryFactoryBean<R, M, ID> {
	public SimpleRepositoryFactoryBean() {
	}

	@Override
	@SuppressWarnings("rawtypes")
	protected RepositoryFactorySupport createRepositoryFactory(EntityManager entityManager) {
		return new SimpleBaseRepositoryFactory(entityManager);
	}
}
