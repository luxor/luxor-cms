package com.luxor.core.repository;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.query.QueryLookupStrategy;

/**
 * 资源仓库工厂类
 * 
 * @auditor XinmingYan @data 2016-7-25 下午4:06:55 @versions 1.0
 * @packagePath com.luxro.core.repository.SimpleBaseRepositoryFactory.java
 */
public class SimpleBaseRepositoryFactory<M, ID extends Serializable> extends JpaRepositoryFactory {

	private EntityManager entityManager;

	public SimpleBaseRepositoryFactory(EntityManager entityManager) {
		super(entityManager);
		this.entityManager = entityManager;
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected Object getTargetRepository(RepositoryMetadata metadata) {
		Class<?> repositoryInterface = metadata.getRepositoryInterface();
		if (isBaseRepository(repositoryInterface)) {
			JpaEntityInformation<M, ID> entityInformation = getEntityInformation((Class<M>) metadata.getDomainType());
			// 自定义仓库资源
			BaseRepositoryImpl repository = new BaseRepositoryImpl<M, ID>(entityInformation, entityManager);
			return repository;
		}
		return super.getTargetRepository(metadata);
	}

	@Override
	protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
		if (isBaseRepository(metadata.getRepositoryInterface())) {
			return BaseRepositoryImpl.class;
		}
		return super.getRepositoryBaseClass(metadata);
	}

	private boolean isBaseRepository(Class<?> repositoryInterface) {
		return BaseRepository.class.isAssignableFrom(repositoryInterface);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected QueryLookupStrategy getQueryLookupStrategy(QueryLookupStrategy.Key key) {
		return super.getQueryLookupStrategy(key);
	}
}