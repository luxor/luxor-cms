package com.luxor.core.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.luxor.core.plugin.search.QueryParam;

/**
 * 原生JDBC核心类 基于jdbc的数据访问对象的超类。
 * 
 * @author admin
 */
public interface CommonDao {
	/**
	 * 返回总记录数
	 * 
	 * @param sql
	 * @param values
	 * @return
	 */
	public abstract long findCount(String sql, Object... values);

	/**
	 * 普通sql查询
	 * 
	 * @param sql
	 *            查询sql
	 * @param classType
	 *            返回值类型
	 * @param values
	 *            条件的参数
	 * @return
	 */
	public abstract List<?> findBySql(String sql, Class<?> classType, final Object... values);

	/**
	 * 普通sql查询
	 * 
	 * @param sql
	 *            查询sql
	 * @param values
	 *            条件的参数
	 * @return
	 */
	public abstract List<Map<String, Object>> findBySql(String sql, final Object... values);

	/**
	 * 带排序的sql查询
	 * 
	 * @param sql
	 *            查询sql
	 * @param sort
	 *            排序条件
	 * @param values
	 *            参数集合
	 * @return
	 */
	public abstract List<Map<String, Object>> findBySql(String sql, Sort sort, Object... values);

	/**
	 * 返回限定条数的数据(分页查询)
	 * 
	 * @param sql
	 * @param limit
	 * @param offset
	 * @param values
	 * @return
	 */
	public abstract List<Map<String, Object>> findBySql(String sql, int limit, int offset, Object... values);

	/**
	 * 带过滤条件的sql查询
	 * 
	 * @param sql
	 *            查询sql
	 * @param filters
	 *            过滤条件表达式集合
	 * @param values
	 *            参数集合
	 * @return
	 */
	public abstract List<Map<String, Object>> findBySql(String sql, List<QueryParam> filters, final Object... values);

	/**
	 * 通过sql查询并排序过滤
	 * 
	 * @param sql
	 *            查询sql
	 * @param filters
	 *            过滤条件
	 * @param sort
	 *            排序条件
	 * @param values
	 *            参数集合
	 * @return
	 */
	public abstract List<Map<String, Object>> findBySql(String sql, List<QueryParam> filters, Sort sort,
			final Object... values);

	/**
	 * 通过sql查询Page页面
	 * 
	 */
	public abstract Page<Map<String, Object>> findPageBySql(Pageable pageable, String sql, final Object... values);

	/**
	 * 通过sql查询Page页数据对象并带排序
	 * 
	 * @param pageable
	 *            页对象
	 * @param sql
	 * @param values
	 * @return
	 */
	public abstract Page<Map<String, Object>> findPageBySql(Pageable pageable, String sql, List<QueryParam> filters,
			final Object... values);

}