package com.luxor.core.repository;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.luxor.core.plugin.search.QueryParam;
import com.luxor.core.plugin.search.SearchHolder;

@Component
public class CommonDaoImpl implements CommonDao {

	private static final String COUNT_SQL = "select count(*) from ( %s ) t";
	private static final String LIMIT_SQL = "select * from ( %s ) t limit  %s , %s";

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Map<String, Object>> findBySql(String sql, final Object... values) {
		List<Map<String, Object>> result = getJdbcTemplate().queryForList(sql, values);
		return result;
	}

	@Override
	public List<?> findBySql(String sql, Class<?> classType, final Object... values) {
		List<?> result = getJdbcTemplate().queryForList(sql, classType, values);
		return result;
	}

	@Override
	public long findCount(String sql, Object... values) {
		Long num = jdbcTemplate.queryForObject(String.format(COUNT_SQL, sql), Long.class, values);
		return num;
	}

	@Override
	public List<Map<String, Object>> findBySql(String sql, Sort sort, Object... values) {
		String sortSql = SearchHolder.appendSortSql(sql, sort);
		return findBySql(sortSql, values);
	}

	@Override
	public List<Map<String, Object>> findBySql(String sql, List<QueryParam> filters, final Object... values) {
		filters = SearchHolder.orderFilters(filters);
		return findBySql(SearchHolder.getFilterSql(sql, filters), SearchHolder.getQueryValues(filters, values));
	}

	@Override
	public List<Map<String, Object>> findBySql(String sql, List<QueryParam> filters, Sort sort, final Object... values) {
		filters = SearchHolder.orderFilters(filters);
		String sortSql = SearchHolder.appendSortSql(SearchHolder.getFilterSql(sql, filters), sort);
		return findBySql(sortSql, SearchHolder.getQueryValues(filters, values));
	}

	@Override
	public Page<Map<String, Object>> findPageBySql(Pageable pageable, String sql, List<QueryParam> filters,
			final Object... values) {
		filters = SearchHolder.orderFilters(filters);
		return findPageBySql(pageable, SearchHolder.getFilterSql(sql, filters), SearchHolder.getQueryValues(filters, values));
	}

	@Override
	public Page<Map<String, Object>> findPageBySql(Pageable pageable, String sql, final Object... values) {
		long size = findCount(sql, values);

		String sortSql = SearchHolder.appendSortSql(sql, pageable.getSort());
		int limit = pageable.getPageNumber() * pageable.getPageSize();
		int offset = pageable.getPageSize();
		List<Map<String, Object>> result = findBySql(sortSql, limit, offset, values);
		return new PageImpl<Map<String, Object>>(result, pageable, size);
	}

	@Override
	public List<Map<String, Object>> findBySql(String sql, int limit, int offset, Object... values) {
		String query = String.format(LIMIT_SQL, new Object[] { sql, limit, offset });
		return findBySql(query, values);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
}
