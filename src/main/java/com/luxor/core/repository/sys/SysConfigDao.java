package com.luxor.core.repository.sys;

import com.luxor.core.entity.sys.SysConfig;
import com.luxor.core.repository.BaseRepository;

public interface SysConfigDao extends BaseRepository<SysConfig, Integer>
{
	public SysConfig getByKey(String key);

	public SysConfig deleteByKey(String key);
}
