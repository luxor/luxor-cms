package com.luxor.core.repository.sys;

import com.luxor.core.entity.sys.SysUser;
import com.luxor.core.repository.BaseRepository;

public interface SysUserDao extends BaseRepository<SysUser, Integer>
{
	
	public SysUser findByUsername(String username);
	
	public SysUser findByUsernameAndStatus(String username, int status);
	
}
