package com.luxor.core.repository.sys;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;

import com.luxor.core.entity.sys.SysPermission;
import com.luxor.core.repository.BaseRepository;

public interface SysPermissionDao extends BaseRepository<SysPermission, Integer>
{
	public SysPermission findByUriAndStatus(String name, int status);
	
	@Query(value = "SELECT p.* FROM T_SYS_PERMISSON as p RIGHT JOIN T_SYS_ROLE_PERMISSION as rp ON rp.permission_id = p.id WHERE rp.role_id=?1 AND p.`status` = 1 ", nativeQuery = true)
	public Collection<SysPermission> findByRoleId(int roleId);

}
