package com.luxor.core.repository.sys;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;

import com.luxor.core.entity.sys.SysRole;
import com.luxor.core.repository.BaseRepository;

public interface SysRoleDao extends BaseRepository<SysRole, Integer>
{
	@Query(value = "select * from T_SYS_ROLE where name = ?1 and status =?2 ", nativeQuery = true)
	public SysRole findByName(String rolename, int status);
	
	@Query(value = "select r.* from T_SYS_ROLE as r RIGHT JOIN T_SYS_GROUP_ROLE as gr ON gr.role_id = r.id WHERE gr.group_id = ?1 AND r.status = 1 ", nativeQuery = true)
	public Collection<SysRole> findByGroupId(int groupid);
}
