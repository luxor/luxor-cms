package com.luxor.core.repository.sys;

import java.util.Collection;

import com.luxor.core.entity.sys.SysUserGroup;
import com.luxor.core.repository.BaseRepository;

public interface SysUserGroupDao extends BaseRepository<SysUserGroup, Integer>
{
	public Collection<SysUserGroup> findByUserId(int userId);
	
	public Collection<SysUserGroup> findByGroupId(int groupId);
}
