package com.luxor.core.repository.sys;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;

import com.luxor.core.entity.sys.SysGroup;
import com.luxor.core.repository.BaseRepository;

public interface SysGroupDao extends BaseRepository<SysGroup, Integer>
{
	public SysGroup findByNameAndStatus(String name, int status);

	@Query(value = "select g.* from T_SYS_GROUP as g RIGHT JOIN T_SYS_USER_GROUP as ug ON ug.group_id = g.id WHERE ug.user_id = ?1 AND g.status = 1 ", nativeQuery = true)
	public Collection<SysGroup> findByUserId(int userId);

}
