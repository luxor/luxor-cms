package com.luxor.core.repository.sys;

import java.util.Collection;

import com.luxor.core.entity.sys.SysGroupRole;
import com.luxor.core.repository.BaseRepository;

public interface SysGroupRoleDao extends BaseRepository<SysGroupRole, Integer> {
	public Collection<SysGroupRole> findByGroupId(int groupId);

	public Collection<SysGroupRole> findByRoleId(int roleId);
}
