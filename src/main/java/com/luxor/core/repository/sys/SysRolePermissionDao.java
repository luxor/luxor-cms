package com.luxor.core.repository.sys;

import java.util.Collection;

import com.luxor.core.entity.sys.SysRolePermission;
import com.luxor.core.repository.BaseRepository;

public interface SysRolePermissionDao extends BaseRepository<SysRolePermission, Integer>
{
	public Collection<SysRolePermission> findByRoleId(int roleId);

	public Collection<SysRolePermission> findByPermissionId(int perissionId);
}
