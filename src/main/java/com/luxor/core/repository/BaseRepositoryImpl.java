package com.luxor.core.repository;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.jpa.criteria.OrderImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.luxor.core.plugin.search.QueryParam;
import com.luxor.core.plugin.search.SearchHolder;

/**
 * Jpa Repository 自定义扩展实现
 * 
 * @auditor XinmingYan @data 2016-7-21 上午12:40:50 @versions 1.0
 * @packagePath com.luxro.cms.repository.SimpleBaseRepository.java
 */
@Repository
@Transactional(readOnly = true)
public class BaseRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID>
		implements BaseRepository<T, ID> {
	
	@Override
	public Long size(List<QueryParam> filters) {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Long> query = builder.createQuery(Long.class);
		Root<T> root = query.from(getEntityClass());
		query.select(builder.count(root));
		Predicate predicate = SearchHolder.getWherePredicate(builder, root, filters);
		if (predicate != null) {
			query.where(predicate);
		}
		return em.createQuery(query).getSingleResult().longValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<T> findAll(Pageable pageable, List<QueryParam> filters) {
		filters = SearchHolder.orderFilters(filters);
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<T> query = builder.createQuery(getEntityClass());
		Root<T> root = query.from(getEntityClass());
		Predicate predicate = SearchHolder.getWherePredicate(builder, root, filters);
		if (predicate != null) {
			query.where(predicate);
		}
		if (pageable.getSort() != null) {
			List<javax.persistence.criteria.Order> orders = new LinkedList<javax.persistence.criteria.Order>();
			for (Order order : pageable.getSort()) {
				orders.add(new OrderImpl(getExpression(root, order.getProperty()), order
						.isAscending()));
			}
			query.orderBy(orders);
		}
		Query hqlQuery = em.createQuery(query);
		hqlQuery.setFirstResult(pageable.getPageSize() * pageable.getPageNumber());
		hqlQuery.setMaxResults(pageable.getPageSize());
		return new PageImpl<T>(hqlQuery.getResultList(), pageable, size(filters));
	}

	@Override
	public List<T> findAll(List<QueryParam> filters, Sort sort) {
		filters = SearchHolder.orderFilters(filters);
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<T> query = builder.createQuery(getEntityClass());
		Root<T> root = query.from(getEntityClass());
		Predicate predicate = SearchHolder.getWherePredicate(builder, root, filters);
		if (predicate != null) {
			query.where(predicate);
		}
		if (sort != null) {
			List<javax.persistence.criteria.Order> orders = new LinkedList<javax.persistence.criteria.Order>();
			for (Order order : sort) {
				orders.add(new OrderImpl(getExpression(root, order.getProperty()), order
						.isAscending()));
			}
			query.orderBy(orders);
		}
		return em.createQuery(query).getResultList();
	}

	@Override
	public List<T> findAll(List<QueryParam> filters) {
		return findAll(filters, null);
	}

	@Override
	public Map<Object, Object> getTableMap(String key, String value) {
		Map<Object, Object> result = new HashMap<Object, Object>();
		StringBuilder hql = new StringBuilder("");
		hql.append("select new Map(").append(key).append(" as key,");
		hql.append(value).append(" as value) from ").append(tableName);
		@SuppressWarnings("unchecked")
		List<Map<Object, Object>> list = em.createQuery(hql.toString()).getResultList();
		for (Map<Object, Object> m : list) {
			result.put(m.get("key"), m.get("value"));
		}
		return result;
	}

	@Transactional
	@Override
	public void delete(final ID[] ids) {
		Set<T> models = new HashSet<T>();
		for (ID id : ids) {
			try {
				T model = entityClass.newInstance();
				BeanUtils.setProperty(model, idName, id);
				models.add(model);
			} catch (Exception e) {
				throw new RuntimeException("batch delete " + entityClass + " error", e);
			}
		}
		deleteInBatch(models);
	}

	private Expression<?> getExpression(Root<T> root, String propertyName) {
		EntityType<T> model = root.getModel();
		Class<?> clazz = null;
		for (EntityType<?> type : em.getMetamodel().getEntities()) {
			if (type.getJavaType().equals(getEntityClass())) {
				type.getDeclaredAttribute(propertyName).getJavaType();
				break;
			}
		}
		return root.get(model.getSingularAttribute(propertyName, clazz));
	}

	/**************************************
	 * 
	 * 仓库资源默认支持的部分
	 * 
	 **************************************/
	public static final String DELETE_ALL_QUERY_STRING = "delete from %s x where x in (?1)";

	private final EntityManager em;
	private final JpaEntityInformation<T, ID> entityInformation;
	// 实体类
	private Class<T> entityClass;
	// 实体对应的<表名称>
	private String tableName;
	// 实体对应的<主键字段名>
	private String idName;

	public BaseRepositoryImpl(JpaEntityInformation<T, ID> entityInformation,
			EntityManager entityManager) {
		super(entityInformation, entityManager);
		this.entityInformation = entityInformation;
		this.entityClass = this.entityInformation.getJavaType();
		this.tableName = this.entityInformation.getEntityName();
		this.idName = this.entityInformation.getIdAttributeNames().iterator().next();
		this.em = entityManager;
	}

	public Class<T> getEntityClass() {
		return entityClass;
	}

	public String getEntityName() {
		return tableName;
	}

	public String getIdName() {
		return idName;
	}

	@Override
	public EntityManager getEntityManager() {
		return em;
	}
}
