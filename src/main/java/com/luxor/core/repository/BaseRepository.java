package com.luxor.core.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import com.luxor.core.plugin.search.QueryParam;

/**
 * Jpa Repository 基类
 * 
 * @auditor XinmingYan @data 2016-7-25 下午4:12:05 @versions 1.0
 * @packagePath com.luxro.core.repository.BaseRepository.java
 */
@NoRepositoryBean
public interface BaseRepository<T, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {
	/**
	 * 动态查询
	 * 
	 * @param filters
	 * @return
	 */
	public List<T> findAll(final List<QueryParam> filters);

	public List<T> findAll(final List<QueryParam> filters, Sort sort);

	/**
	 * 动态分页查询
	 * 
	 * @param pageable
	 * @param filters
	 * @return
	 */
	public Page<T> findAll(Pageable pageable, final List<QueryParam> filters);

	/**
	 * 统计结果集大小
	 * 
	 * @param filters
	 * @return
	 */
	public Long size(final List<QueryParam> filters);

	/**
	 * 返回名值对MAP，一般用于构造实体变量的键值对
	 * 
	 * @param clz 实体类型
	 * @param key 主键ID
	 * @param value 成员变量
	 * @return
	 */
	public Map<Object, Object> getTableMap(String key, String value);

	/**
	 * 根据主键批量删除
	 *
	 * @param ids
	 * @return
	 */
	public void delete(ID[] ids);

	/**
	 * 获取EntityManager
	 * 
	 * @return
	 */
	public EntityManager getEntityManager();
}
