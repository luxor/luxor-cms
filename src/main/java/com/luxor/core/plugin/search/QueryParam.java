package com.luxor.core.plugin.search;

import com.luxor.core.exception.sys.ParamIllegalException;
import com.luxor.core.plugin.search.type.OperateType;
import com.luxor.core.plugin.search.type.PredictionType;

/**
 * SQL动态查询中的过滤条件 比如<filter_EQ_username>参数名称，等价于"select * from sysUser
 * username=？"中的"username=？".
 * 
 * @auditor XinmingYan @data 2016-8-29 下午5:03:12 @versions 1.0
 * @packagePath com.luxor.core.support.search.QuerParam.java
 */
public class QueryParam {
	public static final String COMMA = "\\,";
	public static final String SEPARATOR = "_";
	// 辨别<普通查询参数>和<动态SQL查询参数>的前缀标识
	public static final String PREFIX = "filter_";
	public static final String PREFIX_GROUP = "group_";
	// 参数名称
	private String name;
	// 过滤参数表达式 ( < = >.. )
	private OperateType operate = OperateType.EQ;
	// 参数值( value1,value2.. )
	private String value;

	// 关联标识符 ( and or.. )
	private PredictionType prediction = PredictionType.AND;

	// 是否有集合过滤条件 ( and ( "name1" = "value1" and "name2" = "value2"..) )
	private boolean group = false;

	// 集合过滤参数名称
	private String[] names;
	// 集合过滤参数表达式
	private OperateType[] operates;
	// 集合关联标识符
	private PredictionType[] predictions;

	/**
	 * 过滤条件(首个)："name" >= "value"... 用来收集URL?后的过滤条件
	 * 
	 * @param name
	 *            列名
	 * @param operate
	 *            表达式 (EQ LE GE..)
	 * @param value
	 *            参数
	 */
	public QueryParam(String name, String operate, String value) {
		super();
		this.name = name;
		this.operate = getOperateType(operate);
		this.value = value;
	}

	public QueryParam(String name, OperateType operate, String value) {
		super();
		this.name = name;
		this.operate = operate;
		this.value = value;
	}

	/**
	 * 过滤条件(追加)： and "name" = "value"
	 * 
	 * @param name
	 * @param OperateType
	 * @param value
	 * @param predictionType
	 */
	public QueryParam(PredictionType prediction, String name, OperateType operate, String value) {
		this.prediction = prediction;
		this.name = name;
		this.operate = operate;
		this.value = value;
	}

	public QueryParam(String prediction, String name, String operate, String value) {
		this.prediction = getPredictionType(prediction);
		this.name = name;
		this.operate = getOperateType(operate);
		this.value = value;
	}

	/**
	 * 过滤条件组(追加) ：or ("param1">= "?" and "param2" <= "?")
	 * 
	 * @param predictionType AND ( groups.. )
	 * @param groups (AND,GE,param1,OR,LE,param2)
	 * @param value (18,98)
	 */
	public QueryParam(String prediction, String[] groups, String value) {
		int groupfilterSize = groups.length / 3;
		if (groupfilterSize != value.split(COMMA,-1).length) {
			throw new ParamIllegalException("条件组中的参数值设置不正确。(参数个数不匹配)");
		}
		this.prediction = getPredictionType(prediction);
		this.value = value;
		this.names = new String[groupfilterSize];
		this.operates = new OperateType[groupfilterSize];
		this.predictions = new PredictionType[groupfilterSize];
		for (int s = 0; s < groupfilterSize; s++) {
			predictions[s] = getPredictionType(groups[s * 3]);
			operates[s] = getOperateType(groups[s * 3 + 1]);
			names[s] = groups[s * 3 + 2];
		}
		this.group = true;
	}

	// /////////////////////////////////////////////////////////////////////////

	/**
	 * sql表达式 转换成 枚举表达式
	 * 
	 * @param operate
	 *            表达式 (< = >..)
	 * @return OperateType
	 */
	private OperateType getOperateType(String operate) {
		try {
			return Enum.valueOf(OperateType.class, operate);
		} catch (RuntimeException e) {
			throw new IllegalArgumentException("错误比较符:" + operate, e);
		}
	}

	/**
	 * sql关联式 转换成 枚举表达式
	 * 
	 * @param prediction
	 *            表达式 (AND OR)
	 * @return PredictionType
	 */
	private PredictionType getPredictionType(String prediction) {
		try {
			return Enum.valueOf(PredictionType.class, prediction);
		} catch (RuntimeException e) {
			throw new IllegalArgumentException("错误关联符:" + prediction, e);
		}
	}

	// /////////////////////////////////////////////////////////////////////////

	public String getName() {
		return name;
	}

	public OperateType getOperate() {
		return operate;
	}

	public String getValue() {
		return value;
	}

	public PredictionType getPrediction() {
		return prediction;
	}

	public boolean isGroup() {
		return group;
	}

	public String[] getNames() {
		return names;
	}

	public OperateType[] getOperates() {
		return operates;
	}

	public PredictionType[] getPredictions() {
		return predictions;
	}
}
