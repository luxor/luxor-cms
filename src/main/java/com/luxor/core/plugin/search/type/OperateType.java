package com.luxor.core.plugin.search.type;

/**
 * SQL查询语句中,过滤条件的参数表达式
 * 
 * @auditor XinmingYan @data 2016-8-29 下午5:22:03 @versions 1.0
 * @packagePath com.luxor.core.support.search.OperateType.java
 */
public enum OperateType
{
	EQ("EQ", "="), NE("NE", "<>"), LE("LE", "<="), LT("LT", "<"), GE("GE", ">="), GT("GT", ">"), LIKE("LIKE", "LIKE"), IN("IN", "IN");
	// EQ NE..
	public final String value;
	// < = >..
	public final String operater;

	private OperateType(String value, String operater)
	{
		this.value = value;
		this.operater = operater;
	}

}
