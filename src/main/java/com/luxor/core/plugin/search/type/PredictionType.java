package com.luxor.core.plugin.search.type;

/**
 * SQL查询语句中,过滤条件之间的关联标识符
 * 
 * @auditor XinmingYan @data 2016-8-29 下午5:20:52 @versions 1.0
 * @packagePath com.luxor.core.support.search.PredictionType.java
 */
public enum PredictionType {
	/**
	 * 关联‘与’
	 */
	AND("AND"),
	/**
	 * 关联‘或’
	 */
	OR("OR");

	public final String value;

	private PredictionType(String value) {
		this.value = value;
	}
}
