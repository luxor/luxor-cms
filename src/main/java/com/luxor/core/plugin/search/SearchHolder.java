package com.luxor.core.plugin.search;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.SingularAttribute;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.jpa.criteria.CriteriaBuilderImpl;
import org.hibernate.jpa.criteria.ValueHandlerFactory;
import org.hibernate.jpa.criteria.expression.LiteralExpression;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.luxor.core.exception.sys.ServiceException;
import com.luxor.core.plugin.search.type.OperateType;
import com.luxor.core.plugin.search.type.PredictionType;

/**
 * 动态SQL查询小助手
 * 
 * @auditor XinmingYan @data 2016-8-29 下午5:26:50 @versions 1.0
 * @packagePath com.luxor.core.support.search.SearchManage.java
 */
public class SearchHolder {

	/**
	 * 收集url?后所有filter_开头的查询条件,(如：127.0.0.1?filter_LE_age=18&filter_GE_age=120&
	 * group_OR_LE_name_AND_EQ_CODE=小陈,xiaocheng)
	 * 中间是操作符，最后的是字段名称，操作符种类参考OperateType枚举类.
	 * 
	 * @param request
	 * @return
	 */
	public static List<QueryParam> getQueryParams(HttpServletRequest request) {
		List<QueryParam> filterParams = new LinkedList<QueryParam>();
		Enumeration<String> paramNames = request.getParameterNames();
		while (paramNames != null && paramNames.hasMoreElements()) {
			String paramName = paramNames.nextElement();
			String value = request.getParameterValues(paramName)[0];
			if (StringUtils.isNotEmpty(value)) {
				String[] tmp = paramName.split(QueryParam.SEPARATOR);
				if (paramName.startsWith(QueryParam.PREFIX)) {
					if (tmp.length == 3) {
						filterParams.add(new QueryParam(tmp[2], tmp[1], value));
					} else if (tmp.length == 4) {
						filterParams.add(new QueryParam(tmp[1], tmp[3], tmp[2], value));
					}
				} else if (paramName.startsWith(QueryParam.PREFIX_GROUP) && tmp.length > 3) {
					String[] filters = new String[tmp.length - 1];
					for (int i = 1; i < tmp.length; i++) {
						filters[i - 1] = tmp[i];
					}
					filterParams.add(new QueryParam(tmp[1], filters, value));
				}
			}
		}
		return filterParams;
	}

	/**
	 * 拼接sql查询条件
	 * 
	 * @param tableName
	 * @param filters
	 * @return
	 */
	public static String getFilterSql(String sql, List<QueryParam> filters) {
		StringBuilder sb = new StringBuilder("select * from ").append(sql).append(" t where 1 = 1 ");
		for (int i = 0; i < filters.size(); i++) {
			QueryParam queryParam = filters.get(i);
			sb.append(queryParam.getPrediction().value).append(" ");
			if (queryParam.isGroup()) {
				sb.append(" ( ");
				for (int j = 0; j < queryParam.getNames().length; j++) {
					if (j != 0) {
						sb.append(" " + queryParam.getPredictions()[j] + " ");
					}
					appendSingleFilter(sb, queryParam.getNames()[j], queryParam.getOperates()[j],
							queryParam.getValue());
				}
				sb.append(" ) ");
			} else {
				appendSingleFilter(sb, queryParam.getName(), queryParam.getOperate(), queryParam.getValue());
			}
		}
		return sb.toString();
	}

	/**
	 * 获取全部查询条件的参数值
	 * 
	 * @param filters
	 * @return
	 */
	public static Object[] getQueryValues(List<QueryParam> filters, Object... values) {
		List<Object> params = new LinkedList<Object>();
		if (values != null) {
			for (Object value : values) {
				params.add(value);
			}
		}
		for (QueryParam param : filters) {
			if (param.isGroup()) {// 是否有多个条件表达式
				for (int i = 0; i < param.getNames().length; i++) {
					addSingleValue(params, param.getValue().split(QueryParam.COMMA, -1)[i], param.getOperates()[i]);
				}
			} else {
				addSingleValue(params, param.getValue(), param.getOperate());
			}
		}
		return params.toArray();
	}

	// 追加一个where表达式
	public static void appendSingleFilter(StringBuilder sb, String name, OperateType operater, String value) {
		sb.append(name).append(" ").append(operater.operater).append(" ");
		if (operater.equals(OperateType.IN)) {
			int length = value.split(",").length;
			sb.append(" ( ");
			for (int j = 0; j < length; j++) {
				sb.append(" ? ");
				if (j != length - 1) {
					sb.append(",");
				}
			}
			sb.append(" ) ");
		} else {
			sb.append(" ? ");
		}
	}

	// 追加Sort存放的排序字段,实现排序功能
	public static String appendSortSql(String sql, Sort sort) {
		StringBuilder sb = new StringBuilder(sql);
		if (sort != null) {
			int i = 0;
			for (Order order : sort) {
				if (i == 0) {
					sb.append(" order by");
					i++;
				} else {
					sb.append(",");
				}
				sb.append(order.getProperty()).append(" ").append(order.getDirection());
			}
		}
		return sb.toString();
	}

	/**
	 * 格式化QueryParam的顺序
	 * 
	 * @param filters
	 * @return
	 */
	public static List<QueryParam> orderFilters(final List<QueryParam> filters) {
		List<QueryParam> result = new LinkedList<QueryParam>(filters);
		// 把and的filter排前面，or的排后面
		for (int i = 0, j = 0, k = 0; i < filters.size(); i++) {
			if (filters.get(i).getPrediction().equals(PredictionType.AND)) {
				result.set(j++, filters.get(i));
			} else {
				++k;
				result.set(filters.size() - k, filters.get(i));
			}
		}
		return result;
	}

	/**
	 * 转换成 Expression表达式
	 * 
	 * @param builder
	 * @param root
	 * @param filters
	 * @return
	 */
	public static Predicate getWherePredicate(CriteriaBuilder builder, Root<?> root, List<QueryParam> filters) {
		Predicate predicate = null;
		for (QueryParam filter : filters) {
			predicate = getWherePredicate(predicate, builder, root, filter);
		}
		return predicate;
	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////////

	private static void addSingleValue(List<Object> params, String value, OperateType operate) {
		if (operate.equals(OperateType.LIKE)) {
			params.add("%" + value + "%");
		} else if (operate.equals(OperateType.IN)) {
			for (Object subValue : value.split(",")) {
				params.add(subValue);
			}
		} else {
			params.add(value);
		}
	}

	private static Predicate getWherePredicate(Predicate predicates, CriteriaBuilder builder, Root<?> root,
			QueryParam queryParam) {
		Predicate predicate = null;
		if (queryParam.isGroup()) {
			String[] names = queryParam.getNames();
			OperateType[] operates = queryParam.getOperates();
			PredictionType[] predictions = queryParam.getPredictions();
			String[] values = queryParam.getValue().split(QueryParam.COMMA, -1);
			for (int i = 0; i < names.length; i++) {
				if (i == 0) {
					predicate = buildPredicate(builder, root, names[i], operates[i], values[i]);
				} else {
					if (PredictionType.AND.equals(predictions[i])) {
						predicate = builder.and(predicate,
								buildPredicate(builder, root, names[i], operates[i], values[i]));
					} else {
						predicate = builder.or(predicate,
								buildPredicate(builder, root, names[i], operates[i], values[i]));
					}
				}
			}
		} else {
			predicate = buildPredicate(builder, root, queryParam.getName(), queryParam.getOperate(),
					queryParam.getValue());
		}

		if (predicates != null) {
			PredictionType predictionType = queryParam.getPrediction();
			if (PredictionType.OR.equals(predictionType)) {
				predicate = builder.or(predicates, predicate);
			} else {
				predicate = builder.and(predicates, predicate);
			}
		}
		return predicate;

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static Predicate buildPredicate(CriteriaBuilder builder, Root root, String param, OperateType operateType,
			String value) {
		Predicate predicate = null;
		EntityType<?> model = root.getModel();
		Class<?> propertyClass = model.getAttribute(param).getJavaType();

		switch (operateType) {
		case EQ:
			predicate = builder.equal(root.get(param), ConvertUtils.convert(value, propertyClass));
			break;
		case NE:
			predicate = builder.notEqual(root.get(param), ConvertUtils.convert(value, propertyClass));
			break;
		case LIKE:
			predicate = builder.like(root.get(model.getSingularAttribute(param, String.class)), "%" + value + "%");
			break;
		case LE:
			predicate = builder.lessThanOrEqualTo(root.get(model.getSingularAttribute(param, propertyClass)),
					getValueExpression((CriteriaBuilderImpl) builder, value, propertyClass));
			break;
		case LT:
			predicate = builder.lessThan(root.get((SingularAttribute) model.getSingularAttribute(param, propertyClass)),
					getValueExpression((CriteriaBuilderImpl) builder, value, propertyClass));
			break;
		case GE:
			predicate = builder.greaterThanOrEqualTo(
					root.get((SingularAttribute) model.getSingularAttribute(param, propertyClass)),
					getValueExpression((CriteriaBuilderImpl) builder, value, propertyClass));
			break;
		case GT:
			predicate = builder.greaterThan(
					root.get((SingularAttribute) model.getSingularAttribute(param, propertyClass)),
					getValueExpression((CriteriaBuilderImpl) builder, value, propertyClass));
			break;
		case IN:
			List<Object> inList = new ArrayList<Object>();
			for (String subValue : value.split(",")) {
				if (ValueHandlerFactory.isNumeric(propertyClass)) {
					inList.add(ValueHandlerFactory.convert(subValue, (Class<Number>) propertyClass));
				} else {
					inList.add(ConvertUtils.convert(subValue.toString(), propertyClass));
				}
			}
			predicate = root.get(model.getSingularAttribute(param, propertyClass)).in(inList);
			break;
		default:
			throw new ServiceException("过滤条件的参数表达式,不合法");
		}
		return predicate;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Expression getValueExpression(CriteriaBuilderImpl criteriaBuilder, Object value, Class<?> clazz) {
		if (ValueHandlerFactory.isNumeric(clazz)) {
			return new LiteralExpression<>(criteriaBuilder, ValueHandlerFactory.convert(value, (Class<Number>) clazz));
		} else {
			return new LiteralExpression<>(criteriaBuilder, ConvertUtils.convert(value.toString(), clazz));
		}
	}
}
