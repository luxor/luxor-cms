package com.luxor.core.plugin.shiro;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.PermissionsAuthorizationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.luxor.core.entity.ResultVo;
import com.luxor.core.entity.sys.SysPermission;
import com.luxor.core.service.sys.SysPermissionService;
import com.luxor.core.utils.HttpUtils;

/**
 * 动态 URL,授权过滤器
 * 
 * 根据请求的url,判断用户是否拥有该资源的访问权限
 * @auditor XinmingYan @data 2016-8-20 下午9:43:54 @versions 1.0
 * @packagePath com.luxor.core.support.shiro.UrlAuthorizationFilter.java
 */
public class UrlAuthzFilter extends PermissionsAuthorizationFilter
{
	private static final Logger logger = LoggerFactory.getLogger(UrlAuthzFilter.class);

	@Resource
	SysPermissionService sysPerissionService;

	Map<String, String> urlCache = new HashMap<String, String>();

	private void updatePerissionCache()
	{
		urlCache.clear();
		List<SysPermission> perms = sysPerissionService.findAll();
		if (perms == null || perms.size() == 0) {
			logger.error("Error sysPerission is empty!");
		}

		for (SysPermission permission : perms) {
			String uri = permission.getUri();
			String permissionCode = permission.getPermisson();
			this.urlCache.put(uri, permissionCode);
		}
	}

	@Override
	public boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue)
			throws IOException
	{
		if (urlCache == null || urlCache.size() == 0) {
			updatePerissionCache();
		}

		String[] perms = (String[]) mappedValue;
		String uri = ((HttpServletRequest) request).getServletPath();
		String permission = urlCache.get(uri);
		if (permission != null) {
			perms = (String[]) ArrayUtils.add(perms, permission);
		}

		boolean isPermitted = false;

		Subject subject = getSubject(request, response);
		if (ArrayUtils.isNotEmpty(perms)) {
			if (perms.length == 1 && subject.isPermitted(perms[0])) {
				isPermitted = true;
			} else if (subject.isPermittedAll(perms)) {
				isPermitted = true;
			}
		}
		return isPermitted;
	}

	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response)
			throws IOException
	{
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		Subject subject = getSubject(request, response);

		if (HttpUtils.isAjax(httpRequest)) {
			String message = subject.getPrincipal() == null ? "尚未登录或登录已过期" : "没有权限执行该操作!";
			HttpUtils.sendJson(httpResponse, new ResultVo(false, message));
			return false;
		}

		if (subject.getPrincipal() == null) {
			saveRequestAndRedirectToLogin(request, response);
			return false;
		}

		String unauthorizedUrl = getUnauthorizedUrl();
		if (StringUtils.isNotBlank(unauthorizedUrl)) {
			WebUtils.issueRedirect(request, response, unauthorizedUrl);
		} else {
			WebUtils.toHttp(response).sendError(405);
		}
		return false;
	}

}
