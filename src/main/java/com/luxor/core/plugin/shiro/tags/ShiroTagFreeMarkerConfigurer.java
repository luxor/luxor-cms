package com.luxor.core.plugin.shiro.tags;

import java.io.IOException;

import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.jagregory.shiro.freemarker.ShiroTags;

import freemarker.template.TemplateException;

/**
 * 集成shiroTags标签
 * 
 * @auditor XinmingYan @data 2016-9-8 上午10:30:03 @versions 1.0
 * @packagePath com.luxor.core.support.shiro.tags.ShiroTagFreeMarkerConfigurer.java
 */
public class ShiroTagFreeMarkerConfigurer extends FreeMarkerConfigurer
{
	@Override
	public void afterPropertiesSet() throws IOException, TemplateException
	{
		super.afterPropertiesSet();
		this.getConfiguration().setSharedVariable("shiro", new ShiroTags());
	}
}
