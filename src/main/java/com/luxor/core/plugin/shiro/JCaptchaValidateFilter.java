package com.luxor.core.plugin.shiro;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;

import com.luxor.core.exception.sys.JCaptchaValidateException;
import com.luxor.core.plugin.shiro.jcaptcha.JCaptcha;

/**
 * 验证验证码的合法性
 * 
 * @auditor XinmingYan @data 2016-8-21 下午4:27:16 @versions 1.0
 * @packagePath com.luxor.core.support.shiro.JCaptchaValidateFilter.java
 */
public class JCaptchaValidateFilter extends AccessControlFilter
{
	// 是否开启验证码支持
	private boolean jcaptchaEbabled = true;
	// 前台提交的验证码参数名
	private String jcaptchaParam = "jcaptchaCode";
	// 验证失败后存储到的属性名
	private String failureKeyAttribute = "shiroLoginFailure";

	@Override
	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object paramObject) throws Exception
	{
		request.setAttribute("jcaptchaEbabled", jcaptchaEbabled);
		// 判断验证码是否禁用 或不是表单提交（允许访问）
		HttpServletRequest httpServletRequest = WebUtils.toHttp(request);
		if (jcaptchaEbabled == false || !"post".equalsIgnoreCase(httpServletRequest.getMethod()))
		{
			return true;
		}
		return JCaptcha.validateResponse(httpServletRequest, httpServletRequest.getParameter(jcaptchaParam));
	}

	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception
	{
		String errorName = JCaptchaValidateException.class.getName();
		request.setAttribute(failureKeyAttribute, errorName);
		return true;
	}

	public void setJcaptchaEbabled(boolean jcaptchaEbabled)
	{
		this.jcaptchaEbabled = jcaptchaEbabled;
	}

	public void setJcaptchaParam(String jcaptchaParam)
	{
		this.jcaptchaParam = jcaptchaParam;
	}

	public void setFailureKeyAttribute(String failureKeyAttribute)
	{
		this.failureKeyAttribute = failureKeyAttribute;
	}
}
