package com.luxor.core.plugin.shiro.realm;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.ExpiredCredentialsException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

import com.luxor.core.constant.Invalid;
import com.luxor.core.constant.Status;
import com.luxor.core.entity.sys.SysGroup;
import com.luxor.core.entity.sys.SysPermission;
import com.luxor.core.entity.sys.SysRole;
import com.luxor.core.entity.sys.SysUser;
import com.luxor.core.plugin.shiro.User;
import com.luxor.core.service.sys.SysUserService;

/**
 * 安全数据源(Realm)
 * 
 * @auditor XinmingYan @data 2016-7-27 下午3:13:12 @versions 1.0
 * @packagePath com.luxro.core.support.shiro.MyJdbcRealm.java
 */
public class MyJdbcRealm extends AuthorizingRealm {
	@Resource
	private SysUserService sysUserService;

	@Override
	public String getName() {
		return "MyJdbcRealm";
	}

	/**
	 * 判断此 Realm 是否支持此 Token
	 */
	@Override
	public boolean supports(AuthenticationToken token) {
		// 仅支持 UsernamePasswordToken 类型的 Token
		return token instanceof UsernamePasswordToken;
	}

	/**
	 * 权限验证,获取用户授权信息
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		if (principals == null) {
			throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
		}
		User user = (User) principals.getPrimaryPrincipal();
		
		SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
		SysUser currentUser = sysUserService.findByUsername(user.getUsername());
		List<SysGroup> groups = currentUser.getGroups();
		for (SysGroup sysGroup : groups) {
			// 赋予主体角色权限
			Set<SysRole> roles = sysGroup.getRoles();
			for (SysRole sysRole : roles) {
				authorizationInfo.addRole(sysRole.getRole());
				// 赋予主体资源权限
				Set<SysPermission> permissions = sysRole.getPermissions();
				for (SysPermission sysPermission : permissions) {
					authorizationInfo.addStringPermission(sysPermission.getPermisson());
				}
			}
		}
		return authorizationInfo;
	}
	
	/**
	 * 用户验证,获取身份验证相关信息
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		UsernamePasswordToken upToken = (UsernamePasswordToken) token;
		SysUser currentUser = sysUserService.findByUsername(upToken.getUsername());
		Date expireTime = currentUser.getExpireTime();
		if (expireTime != null) {
			long currentTime = System.currentTimeMillis();
			if (currentTime > expireTime.getTime()) {
				currentUser.setInvalid(Invalid.STALE.key);
				sysUserService.save(currentUser);
			}	
		}
		int status = currentUser.getStatus();
		if (!Status.isEnabled(status)) {
			throw new ExcessiveAttemptsException("用户处于[" + Status.getValue(status) + "]状态,无法使用");
		}
		if (!Invalid.isInvalid(currentUser.getInvalid())) {
			throw new ExpiredCredentialsException("用户名密码已过期");
		}
		String password = currentUser.getPassword();
		String salt = currentUser.getSalt();
		User user = new User(currentUser.getUsername(), currentUser.getNickname());
		user.setId(currentUser.getId());
		user.setEmail(currentUser.getEmail());
		user.setPhone(currentUser.getPhone());
		user.setGroups(currentUser.getGroups().stream().map(SysGroup::getName).collect(Collectors.toList()));
		SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user, password, ByteSource.Util.bytes(salt),
				getName());
		return info;
	}

	@Override
	protected Object getAuthenticationCacheKey(PrincipalCollection principals) {
		User user = (User) getAvailablePrincipal(principals);
		return user.getUsername();
	}

	@Override
	protected Object getAuthorizationCacheKey(PrincipalCollection principals) {
		User user = (User) getAvailablePrincipal(principals);
		return user.getUsername();
	}
}
