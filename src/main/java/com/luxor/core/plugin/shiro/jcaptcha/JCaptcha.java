package com.luxor.core.plugin.shiro.jcaptcha;

import java.awt.image.BufferedImage;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.captchastore.FastHashMapCaptchaStore;
import com.octo.captcha.service.image.DefaultManageableImageCaptchaService;
import com.octo.captcha.service.image.ImageCaptchaService;

/**
 * JCaptcha工具类
 * 
 * @auditor XinmingYan @data 2016-8-23 下午3:03:28 @versions 1.0
 * @packagePath com.luxor.core.support.shiro.jcaptcha.JCaptcha.java
 */
public class JCaptcha
{
	private static ImageCaptchaService instance = new DefaultManageableImageCaptchaService(
			new FastHashMapCaptchaStore(), new GMailEngine(), 8, 1000, 750);

	public static ImageCaptchaService getInstance()
	{
		return instance;
	}

	/**
	 * 检查验证码的合法性
	 * @param sessionId
	 * @param userJcaptchaCode
	 * @return
	 */
	public static boolean validateResponse(HttpServletRequest request, String userCaptchaResponse)
	{
		boolean validated = false;
		if (request.getSession(false) == null) {
			return false;
		}
		try {
			String id = request.getSession().getId();
			validated = getInstance().validateResponseForID(id, userCaptchaResponse).booleanValue();
		} catch (CaptchaServiceException e) {
			e.printStackTrace();
		}
		return validated;
	}

	/**
	 * 返回验证码图片流
	 * @param sessionId
	 * @param locale
	 * @return
	 */
	public static BufferedImage getBufferedImage(String sessionId, Locale locale)
	{
		if (sessionId == null || locale == null) {
			return null;
		}

		BufferedImage bi = getInstance().getImageChallengeForID(sessionId, locale);
		return bi;
	}

}
