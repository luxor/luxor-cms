package com.luxor.core.plugin.shiro;

import java.io.Serializable;
import java.util.Deque;
import java.util.concurrent.LinkedBlockingDeque;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.DefaultSessionKey;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.luxor.core.constant.Constants;

/**
 * 控制并发登录人数
 * 
 * @auditor XinmingYan @data 2016-8-18 下午9:04:04 @versions 1.0
 * @packagePath com.luxor.core.support.shiro.OnlineSessionFilter.java
 */
public class OnlineSessionFilter extends AccessControlFilter implements InitializingBean
{
	private static Logger logger = LoggerFactory.getLogger(OnlineSessionFilter.class);

	private SessionManager sessionManager;

	private CacheManager cacheManager;
	private String onlineSessionCacheName = "onlineSessionCache";
	private Cache<String, Deque<Serializable>> cache;

	// 是否强制踢出已经登陆的用户，默认:false
	private boolean kickoutAfter = false;
	// 一个用户的最大在线会话数
	private int maxSession = 1;

	@Override
	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object paramObject) throws Exception
	{
		return false;
	}

	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception
	{
		Subject subject = getSubject(request, response);
		
		// 如果没有登陆,直接进行之后的流程
		if (subject.isAuthenticated() && subject.isRemembered()) {
			return true;
		}

		Session session = subject.getSession();
		Serializable sessionId = session.getId();
		User user = (User) subject.getPrincipal();
		String username = user.getUsername();

		Deque<Serializable> deque = cache.get(username);
		if (deque == null) {
			deque = new LinkedBlockingDeque<Serializable>();
			cache.put(username, deque);
		}

		// 如果队列里没有此sessionId，且用户没有被踢出；放入队列
		if (!deque.contains(sessionId) && session.getAttribute("kickout") == null) {
			deque.addLast(sessionId);
		}

		// 如果队列里的sessionId数超出最大会话数，开始踢人
		while (deque.size() > maxSession) {
			Serializable kickoutSessionId = null;
			if (kickoutAfter) {
				// 踢出后者
				kickoutSessionId = deque.pollLast();
			} else {
				// 踢出前者
				kickoutSessionId = deque.pollFirst();
			}
			Session kickoutSession = null;
			try {
				kickoutSession = sessionManager.getSession(new DefaultSessionKey(kickoutSessionId));
			} catch (UnknownSessionException e) {
				logger.trace("getSession '{}' is null.", kickoutSessionId);
			}
			if (kickoutSession != null) {
				// 设置会话的kickout属性表示踢出了
				kickoutSession.setAttribute("kickout", true);
			}
		}

		// 如果被踢出了，直接退出，重定向到踢出后的地址
		if (session.getAttribute("kickout") != null) {
			subject.logout();
			request.setAttribute(Constants.MSG, "您已经被挤下线。");
			redirectToLogin(request, response);
			return false;
		}
		return true;
	}

	public void setOnlineSessionCacheName(String onlineSessionCacheName)
	{
		this.onlineSessionCacheName = onlineSessionCacheName;
	}

	public void setKickoutAfter(boolean kickoutAfter)
	{
		this.kickoutAfter = kickoutAfter;
	}

	public void setMaxSession(int maxSession)
	{
		this.maxSession = maxSession;
	}

	public void setCacheManager(CacheManager paramCacheManager)
	{
		this.cacheManager = paramCacheManager;
	}

	public void setSessionManager(SessionManager sessionManager)
	{
		this.sessionManager = sessionManager;
	}

	@Override
	public void afterPropertiesSet() throws Exception
	{
		Assert.assertNotNull(cacheManager);

		if (cacheManager == null) {
			logger.error("You CacheManager is null !");
		}
		this.cache = cacheManager.getCache(this.onlineSessionCacheName);
	}

}
