package com.luxor.core.plugin.shiro.jcaptcha;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.servlet.OncePerRequestFilter;

/**
 * 生成验证码图片
 * 
 * @auditor XinmingYan @data 2016-8-23 下午2:59:59 @versions 1.0
 * @packagePath com.luxor.core.support.shiro.jcaptcha.JCaptchaFilter.java
 */
public class JCaptchaImageFilter extends OncePerRequestFilter
{
	@Override
	public void doFilterInternal(ServletRequest request, ServletResponse response, FilterChain chain)
			throws ServletException, IOException
	{
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;

		httpServletResponse.setHeader("Cache-Control", "no-store");
		httpServletResponse.setHeader("Pragma", "no-cache");
		httpServletResponse.setDateHeader("Expires", 0);
		httpServletResponse.setContentType("image/jpeg");

		Subject subject = SecurityUtils.getSubject();
		String sessionId = (String) subject.getSession(true).getId();
		ServletOutputStream out = httpServletResponse.getOutputStream();

		Locale locale = httpServletRequest.getLocale();
		BufferedImage bi = JCaptcha.getBufferedImage(sessionId, locale);
		try {
			ImageIO.write(bi, "jpg", out);
			out.flush();
		} finally {
			out.close();
		}
	}
}
