package com.luxor.core.plugin.cache;

public interface SimpleCache
{
	/**
	 * 添加一个缓冲数据
	 * @param key 字符串的缓存key
	 * @param value 缓冲的缓存数据
	 */
	void set(String key, Object value);

	/**
	 * 缓存一个数据，并指定存活时间
	 * @param key 字符串的缓存key
	 * @param value 缓冲的缓存数据
	 * @param liveTime 存活时间
	 */
	void set(String key, Object value, long liveTime);

	/**
	 * 根据key获取到一直值
	 * @param key 字符串的缓存key
	 * @return
	 */
	Object get(String key);

	/**
	 * 删除一个数据问题
	 * @Description: 	
	 * @param key 字符串的缓存key
	 * @return
	 */
	long delete(String... keys);

	/**
	 * 判断指定key是否在缓存中已经存在
	 * @param key 字符串的缓存key
	 * @return
	 */
	boolean exists(String key);

	/**
	 * 查看有多少数据
	 * @return
	 */
	long dbSize();

	/**
	 * 清空redis所有数据
	 */
	boolean flushDB();

	/**
	 * 检查是否连接成功
	 * @return
	 */
	String ping();

}
