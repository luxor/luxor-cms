package com.luxor.core.plugin.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.Configuration;

import org.junit.Assert;
import org.springframework.stereotype.Component;

/**
 * Ehcache缓存
 * 
 * @author XinmingYan @dataTime 2017年10月22日 下午12:19:00
 */
@Deprecated
@Component
public class EhcacheCacheImpl implements SimpleCache
{
	private static final String DEFAULT_CACHE = "luxor_default";

	private CacheManager cacheManager;

	private Cache ehCache;

	public EhcacheCacheImpl()
	{
		super();
		CacheConfiguration defaultCacheConfiguration = new CacheConfiguration();
		defaultCacheConfiguration.setEternal(true);
		defaultCacheConfiguration.setOverflowToDisk(true);
		defaultCacheConfiguration.setMaxElementsInMemory(10000);
		defaultCacheConfiguration.setMemoryStoreEvictionPolicy("LFU");
		Configuration config = new Configuration();
		config.addDefaultCache(defaultCacheConfiguration);

		cacheManager = CacheManager.create(config);

		// 自定义默认缓存
		ehCache = new Cache(DEFAULT_CACHE, 10000, true, true, Integer.MAX_VALUE, Integer.MAX_VALUE);
		cacheManager.addCache(ehCache);
		Assert.assertNotNull("default EhCache is null ! ", cacheManager.getCache(DEFAULT_CACHE));
	}

	public CacheManager getCacheManager()
	{
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager)
	{
		this.cacheManager = cacheManager;
	}

	public Cache getEhCache()
	{
		return ehCache;
	}

	public void setEhCache(Cache ehCache)
	{
		this.ehCache = ehCache;
	}

	@Override
	public void set(String key, Object value)
	{
		Element element = new Element(key, value);
		ehCache.put(element);
	}

	@Override
	public void set(String key, Object value, long liveTime)
	{
		Element element = new Element(key, value, false, (int) liveTime, Integer.MAX_VALUE);
		ehCache.put(element);
	}

	@Override
	public Object get(String key)
	{
		return ehCache.get(key);
	}

	@Override
	public long delete(String... keys)
	{
		int result = 0;
		for (String key : keys)
		{
			if (ehCache.remove(key))
			{
				result++;
			}
		}
		return result;
	}

	@Override
	public boolean exists(String key)
	{
		return ehCache.isKeyInCache(key);
	}

	@Override
	public long dbSize()
	{
		return ehCache.getSize();
	}

	@Override
	public boolean flushDB()
	{
		ehCache.removeAll();
		return true;
	}

	@Override
	public String ping()
	{
		if (ehCache.isDisabled())
		{
			return "Disabled";
		}
		return "OK";
	}
}
