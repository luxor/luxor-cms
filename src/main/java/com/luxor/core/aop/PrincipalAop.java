package com.luxor.core.aop;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.apache.shiro.SecurityUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import com.luxor.core.annotation.Principal;

/**
 * ActiveUser自定义注解实现类(获取当前登陆的用户信息)
 * 
 * @auditor XinmingYan @data 2016-8-25 下午5:28:06 @versions 1.0
 * @packagePath com.luxor.core.aop.PrincipalAop.java
 */
@Component
@Aspect
public class PrincipalAop {
	
	@Around("execution(* com.luxor..*.web..*.*(..)) && args(..)")
	public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
		MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
		Method method = methodSignature.getMethod();
		Annotation[][] annotations = method.getParameterAnnotations();

		Class<?>[] carray = method.getParameterTypes();
		Object[] args = joinPoint.getArgs();
		for (int i = 0; i < annotations.length; i++) {
			for (Annotation annotation : annotations[i]) {
				if (annotation instanceof Principal) {
					String value = ((Principal) annotation).value();
					setActiveUser(value, carray, args);
				}
			}
		}
		return joinPoint.proceed(args);
	}

	private void setActiveUser(String value, Class<?>[] carray, Object[] args) {
		for (int i = 0; i < carray.length; i++) {
			if (value.length() > 0 && carray[i].getSimpleName().equalsIgnoreCase(value)) {
				args[i] = getActiveUser();
				return;
			}
			if (value.length() <= 0 && args[i] instanceof Principal) {
				args[i] = getActiveUser();
				return;
			}
		}
	}	

	private Object getActiveUser() {
		Object principal = SecurityUtils.getSubject().getPrincipal();
		if (principal == null) {
			return null;
		}
		return principal;
	}
}
