package com.luxor.core.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.luxor.core.exception.sys.ServiceException;
import com.luxor.core.service.ThemeService;
import com.luxor.core.service.sys.SysConfigService;

/**
 * 服务层统一日志处理
 * 
 * @auditor XinmingYan @data 2016-8-7 下午9:48:39 @versions 1.0
 * @packagePath com.luxor.core.aop.LoggerAop.java
 */
@Component
@Aspect
public class ServiceAop {
	private static final Logger logger = LoggerFactory.getLogger(ServiceAop.class);

	private static final Class<?>[] EXCLUDE_SERVICES = { ThemeService.class, SysConfigService.class };

	/**
	 * 统计控制层方法执行时间
	 * 
	 * @param jp
	 * @return
	 * @throws Throwable
	 */
	@Around("execution(* com.luxor..*.service..*.*(..)) && args(..)")
	public Object tracking(ProceedingJoinPoint jp) throws Throwable {
		String classPath = jp.getTarget().getClass().getName();
		String method = jp.getSignature().getName();
		long begin = System.currentTimeMillis();
		Object o = null;
		try {
			o = jp.proceed();
			if (!exclude(classPath) && logger.isDebugEnabled()) {
				long end = System.currentTimeMillis();
				long costTime = end - begin;
				logger.debug("\r\n## Execute Service The: {}.{}(), cost time(ms):'{}'\r\n",
						new Object[] { classPath, method, costTime });
			}
			return o;
		} catch (Exception e) {
			logger.error("Error Service The: {}.{} \r\n message:{}",
					new Object[] { classPath, method, e.getMessage() });
			throw new ServiceException(e);
		}
	}

	private boolean exclude(String classPath) {
		for (Class<?> sClass : EXCLUDE_SERVICES) {
			if (sClass.getName().equals(classPath)) {
				return true;
			}
		}
		return false;
	}
}
