define(["jquery"], function($) {
	
	/**
	 * 全局统一的 Ajax请求类
	 */
	window.$http = {
		"get" : function(url, params, onSuccess) {
			$.ajax({
				url : url,
				data : params,
				type : "get",
				success : function(result) {
					if(result.success){
						if( typeof(onSuccess) === 'function' ){
							onSuccess(result.data);
						}
					} else {
						$alert.error(result.errors);
						console.error( "[" + url + "] 请求失败, (" +result.code+ ")" + result.errors );
					}
				}
			});
		},
		"post" : function(url, params, onSuccess) {
			$.ajax({
				url : url,
				data : params,
				type : "post",
				success : function(result) {
					if(result.success){
						if( typeof(onSuccess) === 'function' ){
							onSuccess(result.data);
						}
					} else {
						$alert.error(result.errors);
						console.error( "[" + url + "] 请求失败, (" +result.code+ ")" + result.errors );
					}
				}
			});
		},
		"submit" : function(url, formId, onSuccess) {
			$.ajax({
				url : url,
				data : $(formId).serialize(),
				type : "post",
				dataType: "json",
				success : function(result) {
					if(result.success){
						if( typeof(onSuccess) === 'function' ){
							onSuccess(result.data);
						}
					} else {
						$alert.error(result.errors);
						console.error( "[" + url + "] 请求失败, (" +result.code+ ")" + result.errors );
					}
				}
			});
		}
	};
	
	/**
	 * 全局统一的 Alert警告框
	 */
	window.$alert = {
		success : function(value) {
			$("#alert").remove();
			$("body").after("<div id='alert' class='alert alert-success' data-dismiss='alert'><strong>ok,</strong>"+ value + "</div>");
			setTimeout(function() {
				$("#alert").remove();
			}, 3000);
		},
		info : function(value) {
			$("#alert").remove();
			$("body").after("<div id='alert' class='alert alert-info' data-dismiss='alert'>"+ value + "</div>");
			setTimeout(function() {
				$("#alert").remove();
			}, 3000);
		},
		warn : function(value) {
			$("#alert").remove();
			$("body").after("<div id='alert' class='alert alert-warning' data-dismiss='alert'><strong>警告，</strong>"+ value + "</div>");
			setTimeout(function() {
				$("#alert").remove();
			}, 3000);
		},
		error : function(value) {
			$("#alert").remove();
			$("body").after("<div id='alert' class='alert alert-danger' data-dismiss='alert'><strong>错误，</strong>"+ value + "</div>");
			setTimeout(function() {
				$("#alert").remove();
			}, 3000);
		}
	};
	
	/**
	 * 全局统一的 Confirm确认对话框
	 * message: 询问的内容,如: 确定要删除该记录吗?
	 * execute： 得到运行许可后要执行的方法
	 */
	window.$confirm = function( message , execute ){
		window.top.$confirmOk =  function(){
			window.top.$("#confirm").modal('hide');
			if(typeof(execute) == 'function'){
				execute();
			}
		};
		var msg = message == 'undefined' ? "可能造成数据丢失,要继续吗?" : message;
		window.top.$("#confirm").remove();
		var element = "<div id='confirm' class='modal fade bs-example-modal-sm' tabindex='-1'> " +
		"<div class='modal-dialog modal-sm'><div class='modal-content'>" +
		"	<div class='modal-header'>" +
		"		<button type='button' class='close' data-dismiss='modal'>&times;</button>" +
		"		<span class='modal-title'><i class='fa fa-exclamation-circle' style='color: #f0ad4e'></i> 危险动作</span>" +
		"	</div>" +
		"	<div class='modal-body'>" + msg + "</div>" +
		"	<div class='modal-footer'>" +
		"		<button type='button' class='btn btn-default btn-sm' data-dismiss='modal'>取消</button> " +
		"		<button type='button' class='btn btn-warning btn-sm' onClick='$confirmOk()'>确认</button>" +
		"	</div>" +
		"</div></div></div>";
		window.top.$("body").after( element );
		window.top.$("#confirm").modal({show:true});
	};
	
	/**
	 * 全局统一的 modal弹出层
	 */
	window.$modal = function( options ){
		// 参数准备
		var param = $.extend({}, {
			title: "标题名称",
			content: "点击确定后，对话框将在 2秒 后关闭。",
			footer : null,
			show: true, // 是否显示弹出层，true-显示; false-隐藏
			remote : null,
			width: "50%",
			height: "40%",
			cancelText: "取消",
			onCancel: function(){}, // 取消操作时触发
			okText:  "确认",
			onOk: function(){},// 确认操作时触发
			onBefore : function(){}, // 打开弹框前触发
			onAfter : function(){} // 关闭弹框后触发,所有的关闭和取消操作都会触发该事件.
		}, options);
		// 构建dom结构
		var element = $("<div id='modal' class='modal fade' tabindex='-1'></div>");
		var dialog = $("<div class='modal-dialog'></div>").css({height:param.height,width:param.width});
		var content = $("<div class='modal-content'></div>");
		var close = $("<button type='button' class='close' data-dismiss='modal'>&times;</button>");
		var title = $("<span class='modal-title'> " + param.title + "</span>");
		var header = $("<div class='modal-header'></div>");
		var body = $("<div class='modal-body'></div>").css({height:param.height,width:param.width});
		var iframe = $("<iframe name='iModal' width='100%' height='100%' src=" + param.remote +" frameborder='0' seamless='seamless'></iframe>");
		title.appendTo(header);
		close.appendTo(header);
		header.appendTo(content);
		if( param.remote == null ) {
			$( param.content ).appendTo( body );
		} else {
			iframe.appendTo( body );
		}
		body.appendTo(content);
		if ( param.footer == null ){
			var footer = $("<div class='modal-footer'>" +
				"<button type='button' class='btn btn-default btn-sm' onClick='$modal.onCancel()'>" + param.cancelText + "</button> " + 
				"<button type='button' class='btn btn-primary btn-sm' onClick='$modal.onOk()'>" + param.okText + "</button>" +
			"</div>");
			footer.appendTo(content);
		}
		content.appendTo(dialog);
		dialog.appendTo(element);
		window.top.$("#modal").remove();
		window.top.$("body").after( element );
		
		// 绑定确定按钮事件
		window.top.$modal.onOk = function(){
			param.onOk();
		};
		// 绑定取消按钮事件
		window.top.$modal.onCancel = function(){
			param.onCancel();
			window.top.$modal.hide();
		};
		// 显示
		window.top.$modal.show = function(){
			// 绑定关闭后的回调方法
			window.top.$('#modal').on('hidden.bs.modal', function (e) {
			  param.onAfter(window.top);
			  window.top.$("#modal").remove();
			});
			// 绑定打开后的回调方法
			window.top.$('#modal').on('shown.bs.modal', function (e) {
			  param.onBefore(window.top);
			});
			window.top.$("#modal").modal('show');
		};
		// 隐藏
		window.top.$modal.hide = function(){
			window.top.$("#modal").modal('hide');
		};
		// 默认打开或关闭
		if( param.show ){
			window.top.$modal.show();
		} else {
			window.top.$modal.hide();
		}
	}

})