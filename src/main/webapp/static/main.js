require.config({
	urlArgs: "v=" +  new Date().getTime(),// 禁用缓存,生产环境去除
	waitSeconds: 30,
	map:{
		"*": {
			"css": BASE_PATH +"/static/css.min.js"
		}
	},
	shim : {
    	"jquery": { exports: "jQuery" },
        "bootstrap" : { deps: ["jquery"] },
        "bootstrap-validate": { deps: ["bootstrap-validate-core","css!bootstrap-validate-css"] },
        "bootstrap-validate-core" :{ deps: ["jquery","bootstrap"] },
        "bootstrap-table" : { deps: ["bootstrap-table-core","css!bootstrap-table-css"] },
        "bootstrap-table-core": { deps: ["jquery","bootstrap"] },
        "metisMenu" : { deps: ["jquery","bootstrap"] },
        "slimscroll": { deps: ["jquery"] },
        "pace" : { deps: ["jquery"] },
        "zTree" : { deps: ["jquery","css!zTree-css"] }
    },
    paths : {
        "jquery" : BASE_PLUGINS_PATH + "/jquery/js/jquery.min",
        "bootstrap" : BASE_PLUGINS_PATH + "/bootstrap/js/bootstrap.min", 
        "bootstrap-validate" : BASE_PLUGINS_PATH + "/bootstrap-validate/js/language/zh_CN",
        "bootstrap-validate-core" : BASE_PLUGINS_PATH + "/bootstrap-validate/js/bootstrapValidator.min",
        "bootstrap-validate-css" : BASE_PLUGINS_PATH + "/bootstrap-validate/css/bootstrapValidator.min",
        "bootstrap-table": BASE_PLUGINS_PATH + "/bootstrap-table/js/language/zh-CN.min",
        "bootstrap-table-core" : BASE_PLUGINS_PATH + "/bootstrap-table/js/bootstrap-table.min",
        "bootstrap-table-css": BASE_PLUGINS_PATH + "/bootstrap-table/css/bootstrap-table.min",
        "metisMenu" :  BASE_PLUGINS_PATH + "/metisMenu/metisMenu.min",
        "slimscroll" :  BASE_PLUGINS_PATH + "/slimscroll/jquery.slimscroll.min",
        "pace" :  BASE_PLUGINS_PATH + "/pace/pace.min",
        "zTree" : BASE_PLUGINS_PATH + "/zTree/js/jquery.ztree.all.min",
        "zTree-css" : BASE_PLUGINS_PATH + "/zTree/css/zTreeStyle",
        "common": BASE_PATH +"/static/common",
        "mlib": BASE_MANAGE_PATH + "/js",
        "tlib": BASE_TEMPLATE_PATH + "/js",
    }
});

// 加载全局统一的配置
require(["common"]);
