define([ "jquery", "bootstrap", "metisMenu", "slimscroll", "pace" ],function($) {
	/* 设置左侧菜单样式 */
	$("#side-menu").metisMenu();
	/* 设置纵向滚动条样式 */
	$('.sidebar-collapse').slimScroll({
		height : '100%',
		size : '6px'
	});
	/* 自适应内容块的高度值 */
	$(window).bind("load resize", function() {
		var wh = $(window).height();
		$('#content-main').height(wh - 128);
	});

	// 选中某个选项卡
	var check_tag = function(activeTag) {
		$('.J_menuTab').removeClass('active');
		activeTag.addClass('active');
		// 移动选项卡
		move_tag_hander(activeTag);
		// 加载iframe内容
		var id = activeTag.data('id');
		$('.J_iframe').attr('src', id);
	};

	// 移动选项卡逻辑
	var move_tag_hander = function(activeTag) {
		if (activeTag.index() == 0)
			return;
		// 当前选中的选项卡
		var tagOffset = activeTag.offset().left;
		var tagRight = $(window).width() - 250;
		var tagLeft = $('.page-tabs').offset().left;
		var offset = parseInt($(".page-tabs-content")
				.css("margin-left"));
		if (tagOffset > tagRight) {
			// 左移一个选项卡
			$(".page-tabs-content").animate({
				marginLeft : offset - activeTag.outerWidth() + "px"
			}, "fast");
		} else if (tagOffset < tagLeft) {
			// 右移一个选项卡
			$(".page-tabs-content").animate({
				marginLeft : offset + activeTag.outerWidth() + "px"
			}, "fast");
		}
	};

	// 添加选项卡
	var addTag = function(name, data_id) {
		var this_tag = $('.J_menuTab[data-id="' + data_id + '"]');
		// 如果不存在该选项卡，就创建新的选项卡，否则选择以存在的选项卡
		if (this_tag.length == 0) {
			// 添加DOM
			var tag_html = "<a href='javascript:;' class='J_menuTab' data-id='"
					+ data_id
					+ "'>"
					+ name
					+ " <i class='fa fa-times-circle'></i></a>";
			$('.page-tabs-content').append(tag_html);
			this_tag = $('.J_menuTab[data-id="' + data_id + '"]');
		}
		// 选中想添加的选项卡
		check_tag(this_tag);
		// 选项卡选中事件
		this_tag.on('click', function(event) {
			check_tag($(this));
		});
		// 选项卡关闭事件
		this_tag.find('i').on('click', function(event) {
			$(this).parent().remove();
			check_tag($('.J_menuTab:last'));
		});
	};

	// 添加新选项卡的菜单按钮
	$('.J_menuItem').on('click', function(event) {
		var name = $.trim($(this).text());
		var id = $(this).data('id');
		addTag(name, id);
	});

	// 关闭当前选项卡
	$('.J_tabShowActive').on('click', function() {
		var tag = $('.J_menuTab').filter('.active');
		if (tag.index() != 0) {
			tag.remove();
			check_tag($('.J_menuTab:last'));
		}
	});

	// 关闭全部选项卡
	$('.J_tabCloseAll').on('click', function() {
		if ($('.J_menuTab').size() > 1) {
			$('.J_menuTab:not(:first)').remove();
			check_tag($('.J_menuTab:last'));
		}
	});

	// 关闭其它选项卡
	$('.J_tabCloseOther').on('click', function() {
		$('.J_menuTab:not(:first)').not('.active').remove();
	});

	// 切换到上一个选项卡
	$('.J_tabLeft').on('click', function() {
		var iActive = $('.J_menuTab').filter('.active').index();
		var iFirst = $('.J_menuTab:first').index();
		if (iActive > iFirst) {
			var index = iActive - 1;
			var upTag = $('.J_menuTab:eq(' + index + ')');
			check_tag(upTag);
		}
	});

	// 切换到下一个选项卡
	$('.J_tabRight').on('click', function() {
		var iActive = $('.J_menuTab').filter('.active').index();
		var iLast = $('.J_menuTab:last').index();
		if (iActive < iLast) {
			var index = iActive + 1;
			var nextTag = $('.J_menuTab:eq(' + index + ')');
			check_tag(nextTag);
		}
	});

	// 首页选项卡切换事件
	$('.J_menuTab:first').on('click', function(event) {
		$('.J_menuTab').removeClass('active');
		check_tag($(this));
	});

	// 主页菜单
	$('.J_menuItem_home').on('click', function(event) {
		check_tag($('.J_menuTab:first'));
	});

	// 初始化选项卡位置
	$('.page-tabs-content').css("margin-left", "0");
	return $;
})