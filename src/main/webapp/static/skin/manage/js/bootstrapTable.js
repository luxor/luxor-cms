define(["jquery", "bootstrap-table"],function($) {
	return {
		bootstrapTable : function( params ) {
			$(params.el).bootstrapTable('destroy');
			$(params.el).bootstrapTable({
				method : "get",
				url : params.url,
				columns : params.columns,
				striped : true, 
				sortable: true,
				sidePagination : "server",// 服务器分页
				pagination : true, // 启用分页
				pageSize : 10, // 每页显示的记录数
				pageNumber : 1, // 当前第几页
				pageList : [5, 10, 25], // 记录数可选列表
				responseHandler : function(res) {
					return {
						"total" : res.data.total,// 总页数
						"rows" : res.data.rows
						// 数据
					};
				},
				queryParamsType: 'undefined', // undefined
				onDblClickRow : function(row, el) {
					console.log("执行双击某一行的触发事件,row:" + row);
					params.onDblClickRow(row, el);
				}
			});
		}
	}
});