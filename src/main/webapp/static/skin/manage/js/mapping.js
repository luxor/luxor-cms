define(["jquery"], function($) {
	/**
	 * 根据Value获取Name
	 */
	window.$getMappingName = function( choices, value ) {
		for( index in choices ){
			if( choices[index].value == value ){
				return choices[index].name;
			}
		}
		console.warn("Mapping中找不到value=" + name + "的name值");
		return value;
	};
	
	/**
	 * 根据Name获取Value
	 */
	window.$getMappingValue = function( choices, name ) {
		for( index in choices ){
			if( choices[index].name == name ){
				return choices[index].value;
			}
		}
		console.warn("Mapping中找不到name=" + name + "的value值");
		return value;
	};
	
	return {
		/**
		 * 状态
		 */
		status : [
			{ value: 0, name: "失效" }, 
			{ value: 1, name: "正常" }, 
			{ value: 2, name: "待审核" }
		],
		/**
		 * 权限类型
		 */
		permissonType : [
			{ value: "link", name: "链接" }, 
			{ value: "menu", name: "菜单" },
			{ value: "button", name: "按钮" }, 
		],
		/**
		 * 有效期状态
		 */
		invalid : [
			{ value: 1, name: "已过期" }, 
			{ value: 0, name: "有效" }
		],
		/**
		 * 删除状态
		 */
		isDel : [
			{ value: 1, name: "可删除" }, 
			{ value: 0, name: "不可删除" }
		],
		/**
		 * 版本类型
		 */
		version : [
			{ value: 1, name: "新增" }, 
			{ value: 2, name: "修复" }, 
			{ value: 3, name: "优化" }, 
			{ value: 4, name: "删除" }
		]
	}
});