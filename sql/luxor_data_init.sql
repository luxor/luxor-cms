﻿/*
SQLyog Ultimate v11.24 (64 bit)
MySQL - 5.6.39-log : Database - luxorcms
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`luxor-cms` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `luxor-cms`;

/*Table structure for table `advert` */

CREATE TABLE IF NOT EXISTS `advert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `high` int(11) DEFAULT NULL,
  `isBanner` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_9oql3ho8ctgyr34dv0xl6rh81` (`folder_id`),
  CONSTRAINT `FK_9oql3ho8ctgyr34dv0xl6rh81` FOREIGN KEY (`folder_id`) REFERENCES `folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `advert` */

/*Table structure for table `article` */

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_count` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  `view_count` int(11) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ikfj7g3caq8ulhylsei558ef7` (`folder_id`),
  KEY `FK_tcgc5bv7iarylc19wy17n3r6c` (`user_id`),
  CONSTRAINT `FK_ikfj7g3caq8ulhylsei558ef7` FOREIGN KEY (`folder_id`) REFERENCES `folder` (`id`),
  CONSTRAINT `FK_tcgc5bv7iarylc19wy17n3r6c` FOREIGN KEY (`user_id`) REFERENCES `T_SYS_USER` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `article` */

/*Table structure for table `comment` */

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_i4gfabcdddfcsr8rqvwh87hny` (`article_id`),
  KEY `FK_mxoojfj9tmy8088avf57mpm02` (`user_id`),
  CONSTRAINT `FK_i4gfabcdddfcsr8rqvwh87hny` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`),
  CONSTRAINT `FK_mxoojfj9tmy8088avf57mpm02` FOREIGN KEY (`user_id`) REFERENCES `T_SYS_USER` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `comment` */

/*Table structure for table `file` */

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` tinyblob,
  `create_time` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `aricle_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_25atray9at1enkeig4pdf34vk` (`aricle_id`),
  KEY `FK_6g4ifscitfdsrpe3l9gf6bcou` (`user_id`),
  KEY `FK_9wb512d6vikxkvy59vut6if9n` (`image_id`),
  CONSTRAINT `FK_25atray9at1enkeig4pdf34vk` FOREIGN KEY (`aricle_id`) REFERENCES `article` (`id`),
  CONSTRAINT `FK_6g4ifscitfdsrpe3l9gf6bcou` FOREIGN KEY (`user_id`) REFERENCES `T_SYS_USER` (`id`),
  CONSTRAINT `FK_9wb512d6vikxkvy59vut6if9n` FOREIGN KEY (`image_id`) REFERENCES `article` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `file` */

/*Table structure for table `folder` */

CREATE TABLE IF NOT EXISTS `folder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `details` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ename` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `folder` */

/*Table structure for table `guestbook` */

CREATE TABLE IF NOT EXISTS `guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` date DEFAULT NULL,
  `eamil` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_etcincw7a0ab243tvos6r0kgq` (`folder_id`),
  KEY `FK_4u8v39vqp2tuvrqpr467jchpt` (`user_id`),
  CONSTRAINT `FK_4u8v39vqp2tuvrqpr467jchpt` FOREIGN KEY (`user_id`) REFERENCES `T_SYS_USER` (`id`),
  CONSTRAINT `FK_etcincw7a0ab243tvos6r0kgq` FOREIGN KEY (`folder_id`) REFERENCES `folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `guestbook` */

/*Table structure for table `image` */

CREATE TABLE IF NOT EXISTS `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` date DEFAULT NULL,
  `image` tinyblob,
  `status` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `aricle_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_5pgx6508ww0j5u69rbvr1li6i` (`aricle_id`),
  KEY `FK_gn0kkmw9cx9tbd2bwc6xxbqr7` (`user_id`),
  KEY `FK_mp9ykr8o4yc6xwgoln6cdajxt` (`image_id`),
  CONSTRAINT `FK_5pgx6508ww0j5u69rbvr1li6i` FOREIGN KEY (`aricle_id`) REFERENCES `article` (`id`),
  CONSTRAINT `FK_gn0kkmw9cx9tbd2bwc6xxbqr7` FOREIGN KEY (`user_id`) REFERENCES `T_SYS_USER` (`id`),
  CONSTRAINT `FK_mp9ykr8o4yc6xwgoln6cdajxt` FOREIGN KEY (`image_id`) REFERENCES `article` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `image` */

/*Table structure for table `T_SYS_CONFIG` */

CREATE TABLE IF NOT EXISTS `T_SYS_CONFIG` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `details` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `key` varchar(255) COLLATE utf8_bin NOT NULL,
  `value` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `isdel` int(11) DEFAULT NULL,
  `canDel` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `T_SYS_CONFIG` */

insert  into `T_SYS_CONFIG`(`id`,`details`,`key`,`value`,`isdel`,`canDel`) values (1,'网站名称','luxor_seo_title','LuxorCms信息网站',1,0);
insert  into `T_SYS_CONFIG`(`id`,`details`,`key`,`value`,`isdel`,`canDel`) values (2,'网站关键字','luxor_seo_keywords','Luxor|LuxorCms|CMS内容管理系统',1,0);
insert  into `T_SYS_CONFIG`(`id`,`details`,`key`,`value`,`isdel`,`canDel`) values (3,'网站描述','luxor_seo_description','LuxorCms是开源的内容管理系统',1,0);
insert  into `T_SYS_CONFIG`(`id`,`details`,`key`,`value`,`isdel`,`canDel`) values (4,'公司地址','luxor_company_address','广州市天河区科韵路',1,0);
insert  into `T_SYS_CONFIG`(`id`,`details`,`key`,`value`,`isdel`,`canDel`) values (5,'公司名称','luxor_company_name','Luxor',1,0);
insert  into `T_SYS_CONFIG`(`id`,`details`,`key`,`value`,`isdel`,`canDel`) values (6,'管理员邮箱','luxor_admin_email','705673261@qq.com',1,0);
insert  into `T_SYS_CONFIG`(`id`,`details`,`key`,`value`,`isdel`,`canDel`) values (7,'网站备案','luxor_miibeian','粤ICP备xxxxx号',1,0);
insert  into `T_SYS_CONFIG`(`id`,`details`,`key`,`value`,`isdel`,`canDel`) values (8,'模板名称','luxor_theme','default',1,0);

/*Table structure for table `T_SYS_GROUP` */

CREATE TABLE IF NOT EXISTS `T_SYS_GROUP` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `details` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `group` varchar(255) COLLATE utf8_bin NOT NULL,
  `isdel` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `T_SYS_GROUP` */

insert  into `T_SYS_GROUP`(`id`,`details`,`group`,`isdel`,`name`,`sort`,`status`,`pid`) values (1,'负责信息化系统维护','information',1,'信息部',2,1,0);
insert  into `T_SYS_GROUP`(`id`,`details`,`group`,`isdel`,`name`,`sort`,`status`,`pid`) values (2,'拥有内容发布权限','business',NULL,'运营部',1,1,0);
insert  into `T_SYS_GROUP`(`id`,`details`,`group`,`isdel`,`name`,`sort`,`status`,`pid`) values (3,'拥有管理员权限','manager',NULL,'管理办公室',0,1,0);

/*Table structure for table `T_SYS_GROUP_ROLE` */

CREATE TABLE IF NOT EXISTS `T_SYS_GROUP_ROLE` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_c2jqaclpk2lumxv42sh5ba2nc` (`role_id`),
  KEY `FK_7u161itqbecy2h0q2qlaf5564` (`group_id`),
  CONSTRAINT `FK_7u161itqbecy2h0q2qlaf5564` FOREIGN KEY (`group_id`) REFERENCES `T_SYS_GROUP` (`id`),
  CONSTRAINT `FK_c2jqaclpk2lumxv42sh5ba2nc` FOREIGN KEY (`role_id`) REFERENCES `T_SYS_ROLE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `T_SYS_GROUP_ROLE` */

insert  into `T_SYS_GROUP_ROLE`(`id`,`group_id`,`role_id`) values (6,1,1);
insert  into `T_SYS_GROUP_ROLE`(`id`,`group_id`,`role_id`) values (7,2,2);
insert  into `T_SYS_GROUP_ROLE`(`id`,`group_id`,`role_id`) values (9,3,1);

/*Table structure for table `T_SYS_PERMISSION` */

CREATE TABLE IF NOT EXISTS `T_SYS_PERMISSION` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permisson` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `force` int(1) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `isdel` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_rsflmwfgl7ttfoch8utcs2xrq` (`permisson`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `T_SYS_PERMISSION` */

insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (1,'manage:index',0,'Luxor后台管理系统',0,1,1,'link','/manage',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (2,'manage:default',1,'管理控制台',1,2,1,'link','/manage/default',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (3,'manage:changePassword',1,'修改密码',1,3,1,'link','/security/changePassword',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (4,'manage:user',1,'个人资料',1,4,1,'link','/manage/user',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (5,'manage:sys_user',0,'SYS-用户管理',1,5,1,'menu','/manage/sys_user',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (6,'manage:sys_group',0,'SYS-权限管理',1,6,1,'menu','/manage/sys_group',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (7,'manage:sys_role',0,'SYS-角色管理',1,7,1,'menu','/manage/sys_role',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (8,'manage:sys_permission',0,'SYS-权限管理',1,8,1,'menu','/manage/sys_permission',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (9,'manage:sys_config',0,'SYS-系统配置',1,9,1,'menu','/manage/sys_config',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (131,'manage:sys_user:save',0,'保存',5,0,1,'button','/manage/sys_user/save',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (132,'manage:sys_user:delete',0,'删除',5,0,1,'button','/manage/sys_user/delete',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (133,'manage:sys_user:edit',0,'编辑',5,0,1,'button','/manage/sys_user/edit',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (134,'manage:sys_user:view',0,'查看',5,0,1,'button','/manage/sys_user/view',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (135,'manage:sys_user:listAjax',0,'列表数据(ajax)',5,0,1,'button','/manage/sys_user/listAjax',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (136,'manage:sys_permission:save',0,'保存',8,0,1,'button','/manage/sys_permission/save',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (137,'manage:sys_permission:delete',0,'删除',8,0,1,'button','/manage/sys_permission/delete',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (138,'manage:sys_permission:edit',0,'编辑',8,0,1,'button','/manage/sys_permission/edit',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (139,'manage:sys_permission:view',0,'查看',8,0,1,'button','/manage/sys_permission/view',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (140,'manage:sys_permission:listAjax',0,'列表数据(ajax)',8,0,1,'button','/manage/sys_permission/listAjax',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (141,'manage:sys_role:save',0,'保存',7,0,1,'button','/manage/sys_role/save',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (142,'manage:sys_role:delete',0,'删除',7,0,1,'button','/manage/sys_role/delete',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (143,'manage:sys_role:edit',0,'编辑',7,0,1,'button','/manage/sys_role/edit',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (144,'manage:sys_role:view',0,'查看',7,0,1,'button','/manage/sys_role/view',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (145,'manage:sys_role:listAjax',0,'列表数据(ajax)',7,0,1,'button','/manage/sys_role/listAjax',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (146,'manage:sys_group:save',0,'保存',6,0,1,'button','/manage/sys_group/save',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (147,'manage:sys_group:delete',0,'删除',6,0,1,'button','/manage/sys_group/delete',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (148,'manage:sys_group:edit',0,'编辑',6,0,1,'button','/manage/sys_group/edit',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (149,'manage:sys_group:view',0,'查看',6,0,1,'button','/manage/sys_group/view',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (150,'manage:sys_group:listAjax',0,'列表数据(ajax)',6,0,1,'button','/manage/sys_group/listAjax',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (151,'manage:sys_config:save',0,'保存',9,0,1,'button','/manage/sys_config/save',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (152,'manage:sys_config:delete',0,'删除',9,0,1,'button','/manage/sys_config/delete',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (153,'manage:sys_config:edit',0,'编辑',9,0,1,'button','/manage/sys_config/edit',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (154,'manage:sys_config:view',0,'查看',9,0,1,'button','/manage/sys_config/view',NULL);
insert  into `T_SYS_PERMISSION`(`id`,`permisson`,`force`,`name`,`pid`,`sort`,`status`,`type`,`uri`,`isdel`) values (155,'manage:sys_config:listAjax',0,'列表数据(ajax)',9,0,1,'button','/manage/sys_config/listAjax',NULL);

/*Table structure for table `T_SYS_ROLE` */

CREATE TABLE IF NOT EXISTS `T_SYS_ROLE` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `details` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `isdel` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_3g4gom73y90yrownythepa9ps` (`name`),
  UNIQUE KEY `UK_9aast5coq9vjadrg3jor6liaw` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `T_SYS_ROLE` */

insert  into `T_SYS_ROLE`(`id`,`details`,`isdel`,`name`,`role`,`sort`,`status`) values (1,'拥有所有操作的权限',1,'超级管理员','admin',0,1);
insert  into `T_SYS_ROLE`(`id`,`details`,`isdel`,`name`,`role`,`sort`,`status`) values (2,'拥有后台操作的基本权限',NULL,'普通员工','employee',1,1);

/*Table structure for table `T_SYS_ROLE_PERMISSION` */

CREATE TABLE IF NOT EXISTS `T_SYS_ROLE_PERMISSION` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_8xvbv6kex3vpdcg0g0vmr1omo` (`permission_id`),
  KEY `FK_klagu0b6dncvs8ilnuognuk7a` (`role_id`),
  CONSTRAINT `FK_8xvbv6kex3vpdcg0g0vmr1omo` FOREIGN KEY (`permission_id`) REFERENCES `T_SYS_PERMISSION` (`id`),
  CONSTRAINT `FK_klagu0b6dncvs8ilnuognuk7a` FOREIGN KEY (`role_id`) REFERENCES `T_SYS_ROLE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `T_SYS_ROLE_PERMISSION` */

insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (214,4,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (215,2,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (216,3,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (217,1,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (218,135,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (219,134,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (220,5,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (221,133,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (222,132,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (223,131,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (224,144,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (225,142,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (226,140,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (227,138,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (228,8,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (229,136,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (230,6,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (231,155,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (232,153,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (233,151,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (234,149,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (235,147,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (236,145,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (237,143,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (238,141,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (239,139,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (240,137,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (241,9,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (242,7,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (243,154,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (244,152,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (245,150,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (246,148,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (247,146,1);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (253,4,2);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (254,3,2);
insert  into `T_SYS_ROLE_PERMISSION`(`id`,`permission_id`,`role_id`) values (255,2,2);

/*Table structure for table `T_SYS_USER` */

CREATE TABLE IF NOT EXISTS `T_SYS_USER` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` date DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `last_time` date DEFAULT NULL,
  `expire_time` date DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `roleId` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `isdel` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `salt` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `invalid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_h2wsnks66yg8q8elsrpeidcxm` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `T_SYS_USER` */

insert  into `T_SYS_USER`(`id`,`create_time`,`email`,`last_time`,`expire_time`,`password`,`phone`,`roleId`,`username`,`nickname`,`status`,`isdel`,`name`,`salt`,`invalid`) values (1,'2018-08-29','826060129@qq.com',NULL,NULL,'57731ea5cbe71638f3c933e54f32e16a','13112246126',1,'root','Grady91',1,1,'xinmingyan','25055ce0-cecd-4e10-8ff0-f1d0c674e8f5',0);

/*Table structure for table `T_SYS_USER_GROUP` */

CREATE TABLE IF NOT EXISTS `T_SYS_USER_GROUP` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_f99eftu912fxlvhjgx1w3g3k5` (`group_id`),
  KEY `FK_fvugr5ex64es9dl6bwc4w3jum` (`user_id`),
  CONSTRAINT `FK_f99eftu912fxlvhjgx1w3g3k5` FOREIGN KEY (`group_id`) REFERENCES `T_SYS_GROUP` (`id`),
  CONSTRAINT `FK_fvugr5ex64es9dl6bwc4w3jum` FOREIGN KEY (`user_id`) REFERENCES `T_SYS_USER` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `T_SYS_USER_GROUP` */

insert  into `T_SYS_USER_GROUP`(`id`,`group_id`,`user_id`) values (20,1,1);

/*Table structure for table `version` */

CREATE TABLE IF NOT EXISTS `version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t2adl1uke9yqmfiak5juvneah` (`folder_id`),
  CONSTRAINT `FK_t2adl1uke9yqmfiak5juvneah` FOREIGN KEY (`folder_id`) REFERENCES `folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `version` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
